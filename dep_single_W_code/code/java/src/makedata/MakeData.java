package makedata;

import java.io.FileNotFoundException;
import java.util.ArrayList;
import java.util.List;
import java.util.Properties;

import data.PPDBDep;
import edu.illinois.cs.cogcomp.core.io.LineIO;
import edu.stanford.nlp.ling.CoreAnnotations.SentencesAnnotation;
import edu.stanford.nlp.pipeline.Annotation;
import edu.stanford.nlp.pipeline.StanfordCoreNLP;
import edu.stanford.nlp.semgraph.SemanticGraph;
import edu.stanford.nlp.util.CoreMap;

public class MakeData {

	public static void main(String[] args) {
		String trainfile = "/Users/Wieting/Desktop/Paraphrase_Project_data/temp/phrase_training_data_60k.txt";
		PPDBDep ppdb = getDepObject(trainfile);
	}
	
	public static PPDBDep getDepObject(String fname) {
		
		Properties props = new Properties();
		props.put("annotators", "tokenize, ssplit, pos, lemma, parse");
		props.put("tokenize.options", "americanize=true");

		StanfordCoreNLP pipeline = new StanfordCoreNLP(props);
		
		PPDBDep ppdb = new PPDBDep();
		
		try {
			ArrayList<String> lines = LineIO.read(fname);
			for(String s: lines) {
				String[] arr = s.split("\\|\\|\\|");
				String s1 = arr[0];
				String s2 = arr[1];
				makeDeps(s1,s2,pipeline);
			}
		} catch (FileNotFoundException e) {
			// TODO Auto-generated catch block
			e.printStackTrace();
		}
		
		return ppdb;
	}

	public static void makeDeps(String s1, String s2, StanfordCoreNLP pipeline) {
		Annotation document = new Annotation(s1);
		pipeline.annotate(document);
		List<CoreMap> sentences1 = document.get(SentencesAnnotation.class);
		CoreMap sentence1 = sentences1.get(0);

		SemanticGraph dependencies = sentence1.get(edu.stanford.nlp.semgraph.SemanticGraphCoreAnnotations.CollapsedDependenciesAnnotation.class);

		System.out.println(dependencies);
		ArrayList<ArrayList<Double>>	lis = getTree(dependencies,sentence1);
		//makeMatlabTree(dependencies, sentence);
	}

	public static ArrayList<ArrayList<Double>> getTree(
			SemanticGraph dependencies, CoreMap sentence1) {
		
		
	}
}
