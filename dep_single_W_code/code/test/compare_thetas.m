clear;
addpath('../core');
addpath('../obj');
addpath('../../../evaluation/phrases/');
wef = '../../../core_data/skipwiki25.mat';
%initv = '../../../core_data/theta_init_25.mat';
initv = '../../../core_data/theta_init_ones_noise_25.mat';
dataf='../../../core_data/ppdb_test.txt.mat';
hiddenSize=25;
load(initv);
load(wef);
load(dataf);
load('../../../../Paraphrase_Project_data/word_vectors/skipwiki25_1e-05_250_words-xl.params63.mat.ws353sl99.mat');

play_data = [train_data valid_data test_data];
[play_data] = linearize_trees( play_data, theta, hiddenSize, We_orig );

[predicted, gold]=phrases_nn3(theta,We_orig,hiddenSize,words,play_data);

corr(predicted',gold','type','Spearman')