function [t, params] = forwardpassWordDer(params, t, map, default, hiddenSize, We_orig)

[~,sl] = size(t.nums);

words_indexed = t.nums;
size(We_orig);
words_indexed;
words_embedded = We_orig(:, words_indexed);
t.nodeFeaturesforward = zeros(hiddenSize, 2*sl-1);
t.nodeFeaturesforward(:,1:sl) = words_embedded;

W1=[];
W2=[];
bw1=[];

for i = sl+1:2*sl-1
    kids = t.kids(i,:);
    c1 = t.nodeFeaturesforward(:,kids(1));
    c2 = t.nodeFeaturesforward(:,kids(2));
    l=char(t.POS{i});
    if(isKey(map,l))
        idx = map(l);
        theta = params(:,idx);
        W1 = reshape(theta(1:hiddenSize*hiddenSize),hiddenSize,hiddenSize);
        W2 = reshape(theta(hiddenSize*hiddenSize+1:2*hiddenSize*hiddenSize),hiddenSize,hiddenSize);
        bw1 = reshape(theta(2*hiddenSize*hiddenSize+1:2*hiddenSize*hiddenSize+hiddenSize),hiddenSize,1);
    else
        next = length(keys(map))+1;
        map(l) = next;
        W1 = reshape(default(1:hiddenSize*hiddenSize),hiddenSize,hiddenSize);
        W2 = reshape(default(hiddenSize*hiddenSize+1:2*hiddenSize*hiddenSize),hiddenSize,hiddenSize);
        bw1 = reshape(default(2*hiddenSize*hiddenSize+1:2*hiddenSize*hiddenSize+hiddenSize),hiddenSize,1);
        v= next*[W1(:); W2(:); bw1(:)];
        params = [params v];
    end
    p = tanh(W1*c1 + W2*c2 + bw1);
    %p = c1 .* c2;
    %p = (c1+c2)/2;
    %p = vectorsqrt(c1.*c2);
    
    t.nodeFeaturesforward(:,i) = p;
end

end