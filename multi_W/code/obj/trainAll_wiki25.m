clear;
we = '../data/skipwiki25';
initv = '../data/theta_init_25';
data='../data/skipwiki25_s_data2.mat';
hiddenSize = 25;
params.etat=.05;
params.etaw=.1;
params.lambda1=1;
params.lambda2=.0001;
trainTrees2(we,initv,data,hiddenSize,params,'skipwiki25_.05_.1_1_.0001_s');

