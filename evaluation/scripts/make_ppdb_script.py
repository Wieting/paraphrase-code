from glob import glob

dir = '../../models/*mat'
lis = glob(dir)

print "alias matlab=/Applications/MATLAB_R2014a.app/bin/matlab"
for i in lis:
    cmd="matlab -nodisplay -nodesktop -nojvm -nosplash -r \"get_results('"+i+"');quit\""
    print cmd
#call function with filename that writes results
