import sys
import operator

f=open(sys.argv[1],'r')
lines=f.readlines()

MAX=-1
d={}
for i in lines:
	l=i.split('\t')
	if(len(l)==3):
		n=float(l[2])
		d[i.strip()]=n

sorted_d = sorted(d.iteritems(), key=operator.itemgetter(1), reverse=True)

ct=0
for i in sorted_d:
	print i[0]
	ct +=1
	if(ct==MAX):
		break		
