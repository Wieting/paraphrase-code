python gather_results.py > data.txt
sh sorter.sh ppdb_all
sh sorter.sh ppdb_test
sh sorter.sh ppdb_dev

sh sorter.sh sem_trial
sh sorter.sh sem_all

sh sorter.sh dev_verb-noun
sh sorter.sh dev_noun-noun
sh sorter.sh dev_adj-noun
sh sorter.sh test_verb-noun
sh sorter.sh test_noun-noun
sh sorter.sh test_adj-noun
sh sorter.sh all_verb-noun
sh sorter.sh all_noun-noun
sh sorter.sh all_adj-noun

sh sorter.sh all_kj_verb-noun-36-dev
sh sorter.sh all_kj_noun-noun-36-dev
sh sorter.sh all_kj_adj-noun-36-dev
sh sorter.sh all_kj_verb-noun-36-test
sh sorter.sh all_kj_noun-noun-36-test
sh sorter.sh all_kj_adj-noun-36-test

sh sorter.sh all_kj_verb-noun-54-dev
sh sorter.sh all_kj_noun-noun-54-dev
sh sorter.sh all_kj_adj-noun-54-dev
sh sorter.sh all_kj_verb-noun-54-test
sh sorter.sh all_kj_noun-noun-54-test
sh sorter.sh all_kj_adj-noun-54-test

sh sorter.sh all_kj_verb-noun-72-dev
sh sorter.sh all_kj_noun-noun-72-dev
sh sorter.sh all_kj_adj-noun-72-dev
sh sorter.sh all_kj_verb-noun-72-test
sh sorter.sh all_kj_noun-noun-72-test
sh sorter.sh all_kj_adj-noun-72-test

sh sorter.sh all_kj_verb-noun
sh sorter.sh all_kj_noun-noun
sh sorter.sh all_kj_adj-noun
