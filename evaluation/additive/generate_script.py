from glob import glob

lis=glob('../../../Paraphrase_Project_data/word_vectors/*.mat')

print "alias matlab=/Applications/MATLAB_R2014a.app/bin/matlab"
for i in lis:
    cmd="matlab -nodisplay -nodesktop -nojvm -nosplash -r \"evaluate_additive('"+i+"', 'cosine', 'add');quit\""
    print cmd+" > cosine_add/"+i.split("/")[-1].replace('mat','txt')

    cmd="matlab -nodisplay -nodesktop -nojvm -nosplash -r \"evaluate_additive('"+i+"', 'cosine', 'avg');quit\""
    print cmd+" > cosine_avg/"+i.split("/")[-1].replace('mat','txt')

    cmd="matlab -nodisplay -nodesktop -nojvm -nosplash -r \"evaluate_additive('"+i+"', 'dot', 'add');quit\""
    print cmd+" > dot_add/"+i.split("/")[-1].replace('mat','txt')

    cmd="matlab -nodisplay -nodesktop -nojvm -nosplash -r \"evaluate_additive('"+i+"', 'dot', 'avg');quit\""
    print cmd+" > dot_avg/"+i.split("/")[-1].replace('mat','txt')
