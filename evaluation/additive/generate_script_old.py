from glob import glob

lis=glob('../../../Paraphrase_Project_data/word_vectors/*.mat')

print "alias matlab=/Applications/MATLAB_R2014a.app/bin/matlab"
for i in lis:
    cmd="matlab -nodisplay -nodesktop -nojvm -nosplash -r \"evaluate_additive('"+i+"');quit\""
    print cmd+" > "+i.split("/")[-1].replace('mat','txt')
