function [ ] = evaluate_additive( wordfile, type, cmp )

%hack to get voacb if not inclued
load('../../../Paraphrase_Project_data/core_data/skipwiki25.mat');

load(wordfile);
addpath('../../evaluation/bigrams/');
addpath('../../evaluation/wordsim/');
addpath('../../evaluation/semeval/');
addpath('../../single_W_code/code/core/');
fname=wordfile;

%evaluate bigrams
acc = bigrams_additive(We_orig,words,'../../evaluation/bigrams/data/dev_verb-noun.txt',type,cmp);
fprintf('dev_verb-noun.......%s.......%d\n',fname,acc);

acc = bigrams_additive(We_orig,words,'../../evaluation/bigrams/data/dev_noun-noun.txt',type,cmp);
fprintf('dev_noun-noun.......%s.......%d\n',fname,acc);

acc = bigrams_additive(We_orig,words,'../../evaluation/bigrams/data/dev_adj-noun.txt',type,cmp);
fprintf('dev_adj-noun.......%s.......%d\n',fname,acc);

acc = bigrams_additive(We_orig,words,'../../evaluation/bigrams/data/test_verb-noun.txt',type,cmp);
fprintf('test_verb-noun.......%s.......%d\n',fname,acc);

acc = bigrams_additive(We_orig,words,'../../evaluation/bigrams/data/test_noun-noun.txt',type,cmp);
fprintf('test_noun-noun.......%s.......%d\n',fname,acc);

acc = bigrams_additive(We_orig,words,'../../evaluation/bigrams/data/test_adj-noun.txt',type,cmp);
fprintf('test_adj-noun.......%s.......%d\n',fname,acc);

acc = bigrams_additive(We_orig,words,'../../evaluation/bigrams/data/all_verb-noun.txt',type,cmp);
fprintf('all_verb-noun.......%s.......%d\n',fname,acc);

acc = bigrams_additive(We_orig,words,'../../evaluation/bigrams/data/all_noun-noun.txt',type,cmp);
fprintf('all_noun-noun.......%s.......%d\n',fname,acc);

acc = bigrams_additive(We_orig,words,'../../evaluation/bigrams/data/all_adj-noun.txt',type,cmp);
fprintf('all_adj-noun.......%s.......%d\n',fname,acc);

%our bigrams
acc = bigrams_additive(We_orig,words,'../../evaluation/bigrams/data/kj_vn.txt',type,cmp);
fprintf('all_kj_verb-noun.......%s.......%d\n',fname,acc);

acc = bigrams_additive(We_orig,words,'../../evaluation/bigrams/data/kj_nn.txt',type,cmp);
fprintf('all_kj_noun-noun.......%s.......%d\n',fname,acc);

acc = bigrams_additive(We_orig,words,'../../evaluation/bigrams/data/kj_an.txt',type,cmp);
fprintf('all_kj_adj-noun.......%s.......%d\n',fname,acc);

%ours...test
%acc = bigrams_additive(We_orig,words,'../../evaluation/bigrams/data/kj_vn.txt.36.test');
%fprintf('all_kj_verb-noun-36-test.......%s.......%d\n',fname,acc);

%acc = bigrams_additive(We_orig,words,'../../evaluation/bigrams/data/kj_nn.txt.36.test');
%fprintf('all_kj_noun-noun-36-test.......%s.......%d\n',fname,acc);

%acc = bigrams_additive(We_orig,words,'../../evaluation/bigrams/data/kj_an.txt.36.test');
%fprintf('all_kj_adj-noun-36-test.......%s.......%d\n',fname,acc);

%ours...test
%acc = bigrams_additive(We_orig,words,'../../evaluation/bigrams/data/kj_vn.txt.54.test');
%fprintf('all_kj_verb-noun-54-test.......%s.......%d\n',fname,acc);

%acc = bigrams_additive(We_orig,words,'../../evaluation/bigrams/data/kj_nn.txt.54.test');
%fprintf('all_kj_noun-noun-54-test.......%s.......%d\n',fname,acc);

%acc = bigrams_additive(We_orig,words,'../../evaluation/bigrams/data/kj_an.txt.54.test');
%fprintf('all_kj_adj-noun-54-test.......%s.......%d\n',fname,acc);

%ours...test
%acc = bigrams_additive(We_orig,words,'../../evaluation/bigrams/data/kj_vn.txt.72.test');
%fprintf('all_kj_verb-noun-72-test.......%s.......%d\n',fname,acc);

%acc = bigrams_additive(We_orig,words,'../../evaluation/bigrams/data/kj_nn.txt.72.test');
%fprintf('all_kj_noun-noun-72-test.......%s.......%d\n',fname,acc);

%acc = bigrams_additive(We_orig,words,'../../evaluation/bigrams/data/kj_an.txt.72.test');
%fprintf('all_kj_adj-noun-72-test.......%s.......%d\n',fname,acc);

%ours...dev
%acc = bigrams_additive(We_orig,words,'../../evaluation/bigrams/data/kj_vn.txt.36.dev');
%fprintf('all_kj_verb-noun-36-dev.......%s.......%d\n',fname,acc);

%acc = bigrams_additive(We_orig,words,'../../evaluation/bigrams/data/kj_nn.txt.36.dev');
%fprintf('all_kj_noun-noun-36-dev.......%s.......%d\n',fname,acc);

%acc = bigrams_additive(We_orig,words,'../../evaluation/bigrams/data/kj_an.txt.36.dev');
%fprintf('all_kj_adj-noun-36-dev.......%s.......%d\n',fname,acc);

%ours...test
%acc = bigrams_additive(We_orig,words,'../../evaluation/bigrams/data/kj_vn.txt.54.dev');
%fprintf('all_kj_verb-noun-54-dev.......%s.......%d\n',fname,acc);

%acc = bigrams_additive(We_orig,words,'../../evaluation/bigrams/data/kj_nn.txt.54.dev');
%fprintf('all_kj_noun-noun-54-dev.......%s.......%d\n',fname,acc);

%acc = bigrams_additive(We_orig,words,'../../evaluation/bigrams/data/kj_an.txt.54.dev');
%fprintf('all_kj_adj-noun-54-dev.......%s.......%d\n',fname,acc);

%ours...test
%acc = bigrams_additive(We_orig,words,'../../evaluation/bigrams/data/kj_vn.txt.72.dev');
%fprintf('all_kj_verb-noun-72-dev.......%s.......%d\n',fname,acc);

%acc = bigrams_additive(We_orig,words,'../../evaluation/bigrams/data/kj_nn.txt.72.dev');
%fprintf('all_kj_noun-noun-72-dev.......%s.......%d\n',fname,acc);

%acc = bigrams_additive(We_orig,words,'../../evaluation/bigrams/data/kj_an.txt.72.dev');
%fprintf('all_kj_adj-noun-72-dev.......%s.......%d\n',fname,acc);

%evaluate word

%fprintf('evaluating word')
%acc = wordsim(We_orig, words)
%fprintf('%s....%d....%d....%d....%d\n',fname,acc(1),acc(2),acc(3),acc(4));


fprintf('evaluating semeval composition')
corr = semeval_additive(We_orig, '../../evaluation/semeval/SICK_test_annotated.txt.mat',type,cmp);
fprintf('%s....%d\n',fname,corr);


fprintf('evaluating ppdb composition-all')
corr = semeval_additive(We_orig, '../../core_data/3000data.mat',type,cmp);
fprintf('%s....%d\n',fname,corr);

fprintf('evaluating ppdb composition-test')
corr = semeval_additive(We_orig, '../../core_data/ppdb_test.txt.mat',type,cmp);
fprintf('%s....%d\n',fname,corr);

%fprintf('evaluating ppdb composition-1000')
%corr = semeval_additive(We_orig, '../../core_data/3000data.mat');
%fprintf('%s....%d\n',fname,corr);

%TODO bigrams all rank
%TODO bigram OURS

end

