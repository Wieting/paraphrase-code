function [E] = load_embeddings(f)
% f is the filename name holding the embedding vectors
% E is the matrix of word embedding vectors, one row vector for each word.

fid = fopen(f, 'r', 'n', 'UTF-8');
new_size = 10000000; 
a = textscan(fid, '%s', 'Delimiter', '\n', 'BufSize', new_size);
fclose(fid);
bb = cellfun(@str2num, a{1}, 'UniformOutput', false);
E = cell2mat(bb);

%s = size(a{1},1);
%for i=1:s,
%	E(i,:) = str2num(a{1}{i});
%	if mod(i, 2000) == 0,
%		fprintf('%d\n',i);  
%	end
%end
end

