
function [accs] = bigrams(theta,We_orig,hiddenSize,words)
accs = getAcc(theta, We_orig, hiddenSize, words);
end

function [accs] = bigrams2(params, worddir, hiddenSize, last)


load(worddir);

accs=[];

load(params);
acc = getAcc(theta, We_orig, hiddenSize, words);
accs(end+1) = acc

end


function [acc] = getAcc(theta, We_orig, hiddenSize, words)

wordvects = We_orig';
wordvocab = words';
vocab_indices = (1:length(wordvocab))';
wordvocab_map = containers.Map(wordvocab, vocab_indices);

W1 = reshape(theta(1:hiddenSize*hiddenSize),hiddenSize,hiddenSize);
W2 = reshape(theta(hiddenSize*hiddenSize+1:2*hiddenSize*hiddenSize),hiddenSize,hiddenSize);
bw1 = reshape(theta(2*hiddenSize*hiddenSize+1:2*hiddenSize*hiddenSize+hiddenSize),hiddenSize,1);

% Evaluates embeddings.
%
% datadir = directory containing vects and vocab files for embeddings, must end in '/'
% embedding_type = type of the embeddings (suffix of "vects." and "vocab." files)

%datadir = '/usr/local/mbansal/research/dependency_parsing/kevin_cca_eval_scripts/data/';
embed_vocab_file = strcat('vocab.', embedding_type);
embed_vects_file = strcat('vects.', embedding_type);
wordsim_file = 'wordsim353.txt';
sim_file = 'wordsim-sim.txt';
rel_file = 'wordsim-rel.txt';

%%%%%% Read in vocabulary and vector files for embeddings.
[wordvocab,wordvocab_map,wordvects] = load_vocab_and_embeddings(strcat(datadir, embed_vocab_file), strcat(datadir, embed_vects_file));
fprintf('Read in embedding vectors from files %s and %s.\n', embed_vocab_file, embed_vects_file);
fprintf('Found %d words, vects size is %d by %d\n', length(wordvocab), size(wordvects,1), size(wordvects,2));

%%%%%% Read in word similarity dataset.
fid3 = fopen(strcat(datadir, wordsim_file), 'r', 'n', 'UTF-8');
wordsimdata = textscan(fid3,'%s %s %f', 'delimiter','\t', 'HeaderLines', 1);
fclose(fid3);
fprintf('Read in full word similarity dataset from %s\n', wordsim_file);

fid3 = fopen(strcat(datadir, sim_file), 'r', 'n', 'UTF-8');
simdata = textscan(fid3,'%s %s %f', 'delimiter','\t', 'HeaderLines', 1);
fclose(fid3);
fprintf('Read in sim portion of word similarity dataset from %s\n', sim_file);

fid3 = fopen(strcat(datadir, rel_file), 'r', 'n', 'UTF-8');
reldata = textscan(fid3,'%s %s %f', 'delimiter','\t', 'HeaderLines', 1);
fclose(fid3);
fprintf('Read in rel portion of word similarity dataset from %s\n', rel_file);

%%%%%% Compute word similarity metrics.
wordsimwords = [wordsimdata{1}; wordsimdata{2}];
% Look up embeddings for the words in the word similarity dataset.
[relevant_inds, num_unk] = lookup_softmatch(wordvocab_map, wordsimwords, 1);
relevant_vects = wordvects(relevant_inds,:);
fprintf('Num unk wordsim = %d\n', num_unk);
thecorr = compute_sim_corr(relevant_vects, wordsimwords, wordsimdata{1}, wordsimdata{2}, wordsimdata{3});
fprintf('wordsim corr(%s)=%f\n', embedding_type, thecorr);

simwords = [simdata{1}; simdata{2}];
[relevant_inds, num_unk] = lookup_softmatch(wordvocab_map, simwords, 1);
relevant_vects = wordvects(relevant_inds,:);
fprintf('Num unk sim = %d\n', num_unk);
thecorr = compute_sim_corr(relevant_vects, simwords, simdata{1}, simdata{2}, simdata{3});
fprintf('sim corr(%s)=%f\n', embedding_type, thecorr);

relwords = [reldata{1}; reldata{2}];
[relevant_inds, num_unk] = lookup_softmatch(wordvocab_map, relwords, 1);
relevant_vects = wordvects(relevant_inds,:);
fprintf('Num unk rel = %d\n', num_unk);
thecorr = compute_sim_corr(relevant_vects, relwords, reldata{1}, reldata{2}, reldata{3});
fprintf('rel corr(%s)=%f\n', embedding_type, thecorr);

