function [vocab] = load_vocab(f)
% f is the filename holding the vocab
% vocab is the cell array of words

fid = fopen(f, 'r', 'n', 'UTF-8');
vocabtemp = textscan(fid, '%s', 'Delimiter', '\n');
vocab = vocabtemp{1,1};
fclose(fid);
end

