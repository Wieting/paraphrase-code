function [vocab,vocab_map,E] = load_vocab_and_embeddings(vocf, embf)
% vocf is the file holding the vocab
% embf is the file holding the embeddings
% vocab is the cell array of words in vocf
% vocab_map is a map for vocab
% E is the matrix of word embeddings in embf


vocab = load_vocab(vocf);
vocab_indices = (1:length(vocab))';
vocab_map = containers.Map(vocab, vocab_indices);

E = load_embeddings(embf);

% Exit iff the two files don't have the same number of lines.
if (length(vocab) ~= size(E,1)),
    fprintf('Error: length(vocab) != size(E,1)!\n');
    exit;
end
end