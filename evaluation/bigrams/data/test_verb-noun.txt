participant type ver s1lemma1 s1lemma2 s2lemma1 s2lemma2 rating
use knowledge	exercise influence	5
fight war	win battle	5
offer support	provide help	7
develop technique	use power	2
achieve end	close eye	1
meet requirement	help people	3
pour tea	drink water	4
satisfy demand	emphasise need	2
address question	present problem	6
stress importance	cause injury	1
ask man	send message	2
pay price	cut cost	2
consider matter	discuss issue	5
encourage child	leave company	1
shut door	follow road	2
produce effect	start work	4
set example	provide system	6
begin career	suffer loss	1
need treatment	use method	2
pose problem	face difficulty	7
sell property	hold meeting	2
attend conference	require attention	2
read word	receive letter	4
collect information	provide datum	5
win match	reduce amount	2
pass time	cross line	2
hear word	remember name	1
leave house	buy home	2
achieve result	reach level	5
acquire skill	use test	4
lift hand	raise head	5
like people	increase number	1
write book	buy land	6
play game	join party	4
stretch arm	wave hand	3
share interest	express view	7
use knowledge	provide system	3
fight war	win match	6
offer support	cross line	2
pour tea	emphasise need	1
satisfy demand	meet requirement	7
provide datum	reach level	3
follow road	set example	3
buy land	sell property	4
stress importance	cut cost	1
ask man	stretch arm	3
help people	discuss issue	3
raise head	address question	2
achieve result	produce effect	5
consider matter	pose problem	5
join party	leave company	4
shut door	close eye	5
increase number	reduce amount	4
send message	receive letter	4
win battle	play game	5
attend conference	share interest	4
read word	remember name	3
suffer loss	cause injury	3
use test	pass time	3
leave house	encourage child	1
present problem	face difficulty	5
pay price	begin career	2
acquire skill	buy home	4
like people	collect information	1
use power	exercise influence	7
hold meeting	lift hand	2
write book	hear word	3
require attention	need treatment	7
wave hand	start work	2
express view	achieve end	5
drink water	use method	2
provide help	develop technique	5
use knowledge	provide system	3
fight war	win match	4
offer support	cross line	3
pour tea	emphasise need	2
write book	hear word	2
express view	achieve end	5
follow road	set example	5
buy land	sell property	4
stress importance	cut cost	1
ask man	stretch arm	1
help people	discuss issue	4
raise head	address question	2
achieve result	produce effect	7
consider matter	pose problem	3
join party	leave company	4
shut door	close eye	5
increase number	reduce amount	4
send message	receive letter	2
win battle	play game	5
attend conference	share interest	2
read word	remember name	5
suffer loss	cause injury	4
use test	pass time	2
leave house	encourage child	3
present problem	face difficulty	5
pay price	begin career	3
acquire skill	buy home	2
like people	collect information	4
use power	exercise influence	7
hold meeting	lift hand	2
satisfy demand	meet requirement	7
require attention	need treatment	6
wave hand	start work	3
provide datum	reach level	4
drink water	use method	2
provide help	develop technique	6
use knowledge	exercise influence	6
fight war	win battle	6
offer support	provide help	7
develop technique	use power	5
achieve end	close eye	2
meet requirement	help people	5
pour tea	drink water	1
write book	buy land	1
address question	present problem	6
stress importance	cause injury	1
sell property	hold meeting	1
achieve result	reach level	7
consider matter	discuss issue	7
encourage child	leave company	1
shut door	follow road	1
produce effect	start work	5
set example	provide system	6
begin career	suffer loss	2
need treatment	use method	4
pose problem	face difficulty	5
ask man	send message	3
attend conference	require attention	2
read word	receive letter	3
collect information	provide datum	7
win match	reduce amount	1
pass time	cross line	3
hear word	remember name	3
leave house	buy home	2
pay price	cut cost	3
acquire skill	use test	5
lift hand	raise head	7
like people	increase number	1
satisfy demand	emphasise need	4
play game	join party	6
stretch arm	wave hand	6
share interest	express view	6
use knowledge	acquire skill	7
fight war	increase number	1
develop technique	use method	6
achieve end	reach level	6
remember name	pass time	3
pour tea	join party	4
address question	pose problem	6
receive letter	collect information	5
express view	suffer loss	2
start work	begin career	6
buy land	leave house	1
discuss issue	present problem	6
stress importance	emphasise need	6
sell property	buy home	1
help people	encourage child	4
reduce amount	cut cost	6
cause injury	raise head	4
shut door	provide datum	1
cross line	follow road	1
produce effect	consider matter	3
send message	hear word	1
need treatment	offer support	1
win battle	meet requirement	3
attend conference	hold meeting	6
win match	play game	5
provide system	use power	5
achieve result	lift hand	3
close eye	stretch arm	1
face difficulty	set example	4
like people	ask man	1
write book	read word	4
require attention	pay price	1
wave hand	leave company	1
drink water	use test	1
share interest	exercise influence	6
provide help	satisfy demand	7
use knowledge	acquire skill	4
fight war	increase number	1
develop technique	use method	5
achieve end	reach level	7
remember name	pass time	1
pour tea	join party	5
address question	pose problem	5
receive letter	collect information	4
express view	suffer loss	1
start work	begin career	6
buy land	leave house	4
discuss issue	present problem	6
stress importance	emphasise need	7
sell property	buy home	5
help people	encourage child	2
reduce amount	cut cost	6
cause injury	raise head	1
shut door	provide datum	1
cross line	follow road	3
produce effect	consider matter	1
send message	hear word	4
need treatment	offer support	2
win battle	meet requirement	5
attend conference	hold meeting	6
win match	play game	6
provide system	use power	2
achieve result	lift hand	2
close eye	stretch arm	5
face difficulty	set example	4
like people	ask man	2
write book	read word	5
require attention	pay price	1
wave hand	leave company	1
drink water	use test	1
share interest	exercise influence	3
provide help	satisfy demand	6
use knowledge	exercise influence	4
fight war	win battle	5
offer support	provide help	7
develop technique	use power	3
achieve end	close eye	1
meet requirement	help people	1
pour tea	drink water	6
write book	buy land	1
address question	present problem	4
stress importance	cause injury	1
sell property	hold meeting	1
pay price	cut cost	1
consider matter	discuss issue	6
encourage child	leave company	2
shut door	follow road	1
produce effect	start work	1
set example	provide system	3
begin career	suffer loss	1
need treatment	use method	2
pose problem	face difficulty	4
ask man	send message	2
attend conference	require attention	1
read word	receive letter	2
collect information	provide datum	3
win match	reduce amount	1
pass time	cross line	1
hear word	remember name	6
leave house	buy home	2
achieve result	reach level	3
acquire skill	use test	1
lift hand	raise head	2
like people	increase number	1
satisfy demand	emphasise need	1
play game	join party	2
stretch arm	wave hand	2
share interest	express view	5
use knowledge	exercise influence	6
fight war	win battle	7
offer support	provide help	7
develop technique	use power	5
achieve end	close eye	1
meet requirement	help people	3
pour tea	drink water	5
satisfy demand	emphasise need	4
address question	present problem	3
stress importance	cause injury	1
ask man	send message	7
achieve result	reach level	7
consider matter	discuss issue	7
encourage child	leave company	1
shut door	follow road	1
produce effect	start work	4
set example	provide system	5
begin career	suffer loss	2
need treatment	use method	1
pose problem	face difficulty	2
sell property	hold meeting	3
attend conference	require attention	5
read word	receive letter	7
collect information	provide datum	2
win match	reduce amount	1
pass time	cross line	1
hear word	remember name	5
leave house	buy home	3
pay price	cut cost	3
acquire skill	use test	7
lift hand	raise head	3
like people	increase number	7
write book	buy land	1
play game	join party	5
stretch arm	wave hand	5
share interest	express view	7
use knowledge	provide system	3
fight war	win match	5
offer support	cross line	2
pour tea	emphasise need	1
satisfy demand	meet requirement	6
express view	achieve end	2
follow road	set example	1
buy land	sell property	3
stress importance	cut cost	3
ask man	stretch arm	4
help people	discuss issue	4
raise head	address question	5
achieve result	produce effect	5
consider matter	pose problem	4
join party	leave company	3
shut door	close eye	1
increase number	reduce amount	2
send message	receive letter	3
win battle	play game	4
attend conference	share interest	5
read word	remember name	3
suffer loss	cause injury	3
use test	pass time	2
leave house	encourage child	1
present problem	face difficulty	5
pay price	begin career	2
acquire skill	buy home	2
like people	collect information	2
use power	exercise influence	4
hold meeting	lift hand	2
write book	hear word	3
require attention	need treatment	4
wave hand	start work	2
provide datum	reach level	5
drink water	use method	3
provide help	develop technique	3
use knowledge	acquire skill	5
fight war	increase number	3
develop technique	use method	5
achieve end	reach level	7
remember name	pass time	1
pour tea	join party	5
address question	pose problem	5
receive letter	collect information	5
express view	suffer loss	2
start work	begin career	7
buy land	leave house	1
discuss issue	present problem	6
stress importance	emphasise need	7
sell property	buy home	4
help people	encourage child	5
reduce amount	cut cost	7
cause injury	raise head	1
shut door	provide datum	1
cross line	follow road	1
produce effect	consider matter	2
send message	hear word	5
need treatment	offer support	1
win battle	meet requirement	4
attend conference	hold meeting	5
win match	play game	5
provide system	use power	4
achieve result	lift hand	1
close eye	stretch arm	2
face difficulty	set example	5
like people	ask man	1
write book	read word	3
require attention	pay price	2
wave hand	leave company	1
drink water	use test	1
share interest	exercise influence	6
provide help	satisfy demand	6
use knowledge	provide system	3
fight war	win match	6
offer support	cross line	1
pour tea	emphasise need	1
write book	hear word	1
provide datum	reach level	2
follow road	set example	3
buy land	sell property	2
stress importance	cut cost	2
ask man	stretch arm	1
help people	discuss issue	3
raise head	address question	2
pay price	begin career	2
consider matter	pose problem	2
join party	leave company	2
shut door	close eye	2
increase number	reduce amount	1
send message	receive letter	1
win battle	play game	4
attend conference	share interest	3
read word	remember name	2
suffer loss	cause injury	1
use test	pass time	2
leave house	encourage child	1
present problem	face difficulty	1
achieve result	produce effect	7
acquire skill	buy home	1
like people	collect information	1
use power	exercise influence	7
hold meeting	lift hand	1
satisfy demand	meet requirement	7
require attention	need treatment	6
wave hand	start work	1
express view	achieve end	2
drink water	use method	1
provide help	develop technique	1
use knowledge	exercise influence	6
fight war	win battle	4
offer support	provide help	6
develop technique	use power	3
achieve end	close eye	1
meet requirement	help people	2
pour tea	drink water	3
satisfy demand	emphasise need	3
address question	present problem	5
stress importance	cause injury	2
ask man	send message	3
achieve result	reach level	6
consider matter	discuss issue	5
encourage child	leave company	1
shut door	follow road	1
produce effect	start work	2
set example	provide system	6
begin career	suffer loss	1
need treatment	use method	1
pose problem	face difficulty	4
sell property	hold meeting	1
attend conference	require attention	2
read word	receive letter	2
collect information	provide datum	7
win match	reduce amount	1
pass time	cross line	1
hear word	remember name	3
leave house	buy home	2
pay price	cut cost	1
acquire skill	use test	3
lift hand	raise head	4
like people	increase number	1
write book	buy land	1
play game	join party	1
stretch arm	wave hand	2
share interest	express view	6
use knowledge	acquire skill	3
fight war	increase number	3
develop technique	use method	3
achieve end	reach level	5
remember name	pass time	3
pour tea	join party	3
address question	pose problem	3
receive letter	collect information	4
express view	suffer loss	2
start work	begin career	5
buy land	leave house	3
discuss issue	present problem	4
stress importance	emphasise need	5
sell property	buy home	3
help people	encourage child	3
reduce amount	cut cost	6
cause injury	raise head	2
shut door	provide datum	2
cross line	follow road	3
produce effect	consider matter	3
send message	hear word	3
need treatment	offer support	3
win battle	meet requirement	2
attend conference	hold meeting	4
win match	play game	3
provide system	use power	2
achieve result	lift hand	2
close eye	stretch arm	2
face difficulty	set example	2
like people	ask man	2
write book	read word	4
require attention	pay price	2
wave hand	leave company	2
drink water	use test	2
share interest	exercise influence	3
provide help	satisfy demand	4
use knowledge	provide system	5
fight war	win match	2
offer support	cross line	1
pour tea	emphasise need	1
write book	hear word	1
provide datum	reach level	4
follow road	set example	1
buy land	sell property	1
stress importance	cut cost	1
ask man	stretch arm	1
help people	discuss issue	6
raise head	address question	1
achieve result	produce effect	6
consider matter	pose problem	7
join party	leave company	1
shut door	close eye	1
increase number	reduce amount	1
send message	receive letter	1
win battle	play game	2
attend conference	share interest	3
read word	remember name	2
suffer loss	cause injury	2
use test	pass time	1
leave house	encourage child	1
present problem	face difficulty	3
pay price	begin career	1
acquire skill	buy home	1
like people	collect information	1
use power	exercise influence	7
hold meeting	lift hand	1
satisfy demand	meet requirement	7
require attention	need treatment	7
wave hand	start work	1
express view	achieve end	4
drink water	use method	1
provide help	develop technique	4
use knowledge	acquire skill	5
fight war	increase number	1
develop technique	use method	4
achieve end	reach level	4
remember name	pass time	1
pour tea	join party	2
address question	pose problem	4
receive letter	collect information	3
express view	suffer loss	2
start work	begin career	2
buy land	leave house	4
discuss issue	present problem	4
stress importance	emphasise need	5
sell property	buy home	3
help people	encourage child	3
reduce amount	cut cost	7
cause injury	raise head	4
shut door	provide datum	1
cross line	follow road	3
produce effect	consider matter	2
send message	hear word	3
need treatment	offer support	2
win battle	meet requirement	5
attend conference	hold meeting	3
win match	play game	3
provide system	use power	3
achieve result	lift hand	2
close eye	stretch arm	1
face difficulty	set example	4
like people	ask man	2
write book	read word	1
require attention	pay price	3
wave hand	leave company	1
drink water	use test	1
share interest	exercise influence	4
provide help	satisfy demand	3
use knowledge	acquire skill	7
fight war	increase number	1
develop technique	use method	7
achieve end	reach level	4
remember name	pass time	1
pour tea	join party	1
address question	pose problem	7
receive letter	collect information	7
express view	suffer loss	1
start work	begin career	7
buy land	leave house	1
discuss issue	present problem	4
stress importance	emphasise need	7
sell property	buy home	1
help people	encourage child	1
reduce amount	cut cost	7
cause injury	raise head	1
shut door	provide datum	1
cross line	follow road	1
produce effect	consider matter	4
send message	hear word	1
need treatment	offer support	1
win battle	meet requirement	7
attend conference	hold meeting	5
win match	play game	1
provide system	use power	1
achieve result	lift hand	1
close eye	stretch arm	1
face difficulty	set example	4
like people	ask man	1
write book	read word	4
require attention	pay price	5
wave hand	leave company	1
drink water	use test	1
share interest	exercise influence	4
provide help	satisfy demand	7
use knowledge	acquire skill	1
fight war	increase number	1
develop technique	use method	3
achieve end	reach level	3
remember name	pass time	1
pour tea	join party	1
address question	pose problem	4
receive letter	collect information	3
express view	suffer loss	1
start work	begin career	3
buy land	leave house	1
discuss issue	present problem	3
stress importance	emphasise need	7
sell property	buy home	1
help people	encourage child	2
reduce amount	cut cost	6
cause injury	raise head	1
shut door	provide datum	1
cross line	follow road	1
produce effect	consider matter	1
send message	hear word	1
need treatment	offer support	1
win battle	meet requirement	1
attend conference	hold meeting	2
win match	play game	1
provide system	use power	1
achieve result	lift hand	1
close eye	stretch arm	1
face difficulty	set example	1
like people	ask man	1
write book	read word	1
require attention	pay price	1
wave hand	leave company	1
drink water	use test	1
share interest	exercise influence	3
provide help	satisfy demand	1
use knowledge	exercise influence	3
fight war	win battle	5
offer support	provide help	7
develop technique	use power	5
achieve end	close eye	3
meet requirement	help people	2
pour tea	drink water	3
satisfy demand	emphasise need	2
address question	present problem	5
stress importance	cause injury	2
ask man	send message	4
achieve result	reach level	7
consider matter	discuss issue	4
encourage child	leave company	2
shut door	follow road	3
produce effect	start work	3
set example	provide system	5
begin career	suffer loss	2
need treatment	use method	3
pose problem	face difficulty	4
sell property	hold meeting	2
attend conference	require attention	2
read word	receive letter	3
collect information	provide datum	2
win match	reduce amount	2
pass time	cross line	3
hear word	remember name	3
leave house	buy home	3
pay price	cut cost	5
acquire skill	use test	3
lift hand	raise head	2
like people	increase number	2
write book	buy land	2
play game	join party	4
stretch arm	wave hand	5
share interest	express view	5
use knowledge	exercise influence	6
fight war	win battle	3
offer support	provide help	6
develop technique	use power	6
achieve end	close eye	1
meet requirement	help people	1
pour tea	drink water	1
satisfy demand	emphasise need	2
address question	present problem	1
stress importance	cause injury	1
ask man	send message	1
achieve result	reach level	7
consider matter	discuss issue	5
encourage child	leave company	1
shut door	follow road	1
produce effect	start work	4
set example	provide system	1
begin career	suffer loss	1
need treatment	use method	1
pose problem	face difficulty	4
sell property	hold meeting	1
attend conference	require attention	2
read word	receive letter	1
collect information	provide datum	2
win match	reduce amount	1
pass time	cross line	1
hear word	remember name	1
leave house	buy home	1
pay price	cut cost	1
acquire skill	use test	1
lift hand	raise head	1
like people	increase number	1
write book	buy land	1
play game	join party	2
stretch arm	wave hand	2
share interest	express view	7
use knowledge	exercise influence	5
fight war	win battle	5
offer support	provide help	7
develop technique	use power	5
achieve end	close eye	3
meet requirement	help people	4
pour tea	drink water	2
satisfy demand	emphasise need	3
address question	present problem	6
stress importance	cause injury	3
sell property	hold meeting	3
achieve result	reach level	5
consider matter	discuss issue	7
encourage child	leave company	2
shut door	follow road	3
produce effect	start work	5
set example	provide system	3
begin career	suffer loss	2
need treatment	use method	2
pose problem	face difficulty	4
ask man	send message	3
attend conference	require attention	2
read word	receive letter	4
collect information	provide datum	5
win match	reduce amount	2
pass time	cross line	1
hear word	remember name	4
leave house	buy home	2
pay price	cut cost	3
acquire skill	use test	3
lift hand	raise head	4
like people	increase number	1
write book	buy land	2
play game	join party	3
stretch arm	wave hand	5
share interest	express view	7
use knowledge	acquire skill	3
fight war	increase number	1
develop technique	use method	3
achieve end	reach level	5
remember name	pass time	1
pour tea	join party	2
address question	pose problem	5
receive letter	collect information	3
express view	suffer loss	1
start work	begin career	6
buy land	leave house	1
discuss issue	present problem	6
stress importance	emphasise need	7
sell property	buy home	3
help people	encourage child	2
reduce amount	cut cost	7
cause injury	raise head	1
shut door	provide datum	1
cross line	follow road	1
produce effect	consider matter	1
send message	hear word	1
need treatment	offer support	1
win battle	meet requirement	1
attend conference	hold meeting	3
win match	play game	5
provide system	use power	1
achieve result	lift hand	1
close eye	stretch arm	2
face difficulty	set example	1
like people	ask man	1
write book	read word	2
require attention	pay price	1
wave hand	leave company	1
drink water	use test	1
share interest	exercise influence	2
provide help	satisfy demand	4
use knowledge	acquire skill	3
fight war	increase number	1
develop technique	use method	1
achieve end	reach level	5
remember name	pass time	1
pour tea	join party	1
address question	pose problem	3
receive letter	collect information	3
express view	suffer loss	1
start work	begin career	4
buy land	leave house	1
discuss issue	present problem	4
stress importance	emphasise need	6
sell property	buy home	1
help people	encourage child	3
reduce amount	cut cost	6
cause injury	raise head	1
shut door	provide datum	1
cross line	follow road	3
produce effect	consider matter	2
send message	hear word	2
need treatment	offer support	1
win battle	meet requirement	3
attend conference	hold meeting	2
win match	play game	4
provide system	use power	1
achieve result	lift hand	1
close eye	stretch arm	1
face difficulty	set example	2
like people	ask man	1
write book	read word	1
require attention	pay price	1
wave hand	leave company	2
drink water	use test	1
share interest	exercise influence	1
provide help	satisfy demand	3
use knowledge	provide system	4
fight war	win match	5
offer support	cross line	1
pour tea	emphasise need	2
write book	hear word	2
provide datum	reach level	2
follow road	set example	1
buy land	sell property	1
stress importance	cut cost	1
ask man	stretch arm	1
help people	discuss issue	3
raise head	address question	2
achieve result	produce effect	7
consider matter	pose problem	3
join party	leave company	3
shut door	close eye	1
increase number	reduce amount	1
send message	receive letter	2
win battle	play game	4
attend conference	share interest	5
read word	remember name	2
suffer loss	cause injury	1
use test	pass time	3
leave house	encourage child	1
present problem	face difficulty	1
pay price	begin career	1
acquire skill	buy home	3
like people	collect information	1
use power	exercise influence	7
hold meeting	lift hand	2
satisfy demand	meet requirement	6
require attention	need treatment	7
wave hand	start work	2
express view	achieve end	4
drink water	use method	1
provide help	develop technique	2
use knowledge	acquire skill	5
fight war	increase number	1
develop technique	use method	4
achieve end	reach level	5
remember name	pass time	1
pour tea	join party	2
address question	pose problem	5
receive letter	collect information	3
express view	suffer loss	2
start work	begin career	5
buy land	leave house	1
discuss issue	present problem	5
stress importance	emphasise need	5
sell property	buy home	3
help people	encourage child	4
reduce amount	cut cost	5
cause injury	raise head	3
shut door	provide datum	1
cross line	follow road	1
produce effect	consider matter	2
send message	hear word	1
need treatment	offer support	2
win battle	meet requirement	3
attend conference	hold meeting	6
win match	play game	6
provide system	use power	2
achieve result	lift hand	1
close eye	stretch arm	3
face difficulty	set example	3
like people	ask man	2
write book	read word	3
require attention	pay price	3
wave hand	leave company	4
drink water	use test	1
share interest	exercise influence	1
provide help	satisfy demand	3
use knowledge	provide system	1
fight war	win match	1
offer support	cross line	1
pour tea	emphasise need	1
write book	hear word	1
provide datum	reach level	1
follow road	set example	1
buy land	sell property	1
stress importance	cut cost	1
ask man	stretch arm	1
help people	discuss issue	1
raise head	address question	1
pay price	begin career	1
consider matter	pose problem	1
join party	leave company	1
shut door	close eye	1
increase number	reduce amount	1
send message	receive letter	1
win battle	play game	1
attend conference	share interest	1
read word	remember name	1
suffer loss	cause injury	1
use test	pass time	1
leave house	encourage child	1
present problem	face difficulty	1
achieve result	produce effect	2
acquire skill	buy home	1
like people	collect information	1
use power	exercise influence	2
hold meeting	lift hand	1
satisfy demand	meet requirement	2
require attention	need treatment	4
wave hand	start work	1
express view	achieve end	2
drink water	use method	1
provide help	develop technique	1
use knowledge	provide system	5
fight war	win match	5
offer support	cross line	2
pour tea	emphasise need	1
satisfy demand	meet requirement	7
express view	achieve end	5
follow road	set example	1
buy land	sell property	1
stress importance	cut cost	1
ask man	stretch arm	2
help people	discuss issue	5
raise head	address question	4
pay price	begin career	3
consider matter	pose problem	4
join party	leave company	1
shut door	close eye	2
increase number	reduce amount	1
send message	receive letter	1
win battle	play game	4
attend conference	share interest	4
read word	remember name	2
suffer loss	cause injury	1
use test	pass time	3
leave house	encourage child	3
present problem	face difficulty	5
achieve result	produce effect	6
acquire skill	buy home	1
like people	collect information	3
use power	exercise influence	6
hold meeting	lift hand	2
write book	hear word	1
require attention	need treatment	7
wave hand	start work	3
provide datum	reach level	4
drink water	use method	4
provide help	develop technique	6
use knowledge	provide system	3
fight war	win match	2
offer support	cross line	1
pour tea	emphasise need	1
satisfy demand	meet requirement	7
provide datum	reach level	4
follow road	set example	2
buy land	sell property	1
stress importance	cut cost	2
ask man	stretch arm	1
help people	discuss issue	4
raise head	address question	4
pay price	begin career	1
consider matter	pose problem	5
join party	leave company	2
shut door	close eye	2
increase number	reduce amount	1
send message	receive letter	1
win battle	play game	5
attend conference	share interest	4
read word	remember name	3
suffer loss	cause injury	2
use test	pass time	1
leave house	encourage child	1
present problem	face difficulty	4
achieve result	produce effect	5
acquire skill	buy home	1
like people	collect information	3
use power	exercise influence	6
hold meeting	lift hand	2
write book	hear word	3
require attention	need treatment	7
wave hand	start work	3
express view	achieve end	2
drink water	use method	1
provide help	develop technique	3
use knowledge	exercise influence	6
fight war	win battle	6
offer support	provide help	7
develop technique	use power	4
achieve end	close eye	2
meet requirement	help people	5
pour tea	drink water	4
satisfy demand	emphasise need	3
address question	present problem	4
stress importance	cause injury	3
ask man	send message	6
pay price	cut cost	4
consider matter	discuss issue	5
encourage child	leave company	2
shut door	follow road	3
produce effect	start work	5
set example	provide system	4
begin career	suffer loss	2
need treatment	use method	3
pose problem	face difficulty	5
sell property	hold meeting	2
attend conference	require attention	3
read word	receive letter	5
collect information	provide datum	4
win match	reduce amount	2
pass time	cross line	1
hear word	remember name	5
leave house	buy home	1
achieve result	reach level	7
acquire skill	use test	4
lift hand	raise head	3
like people	increase number	4
write book	buy land	2
play game	join party	5
stretch arm	wave hand	6
share interest	express view	7
use knowledge	exercise influence	6
fight war	win battle	5
offer support	provide help	4
develop technique	use power	5
achieve end	close eye	1
meet requirement	help people	4
pour tea	drink water	1
write book	buy land	1
address question	present problem	4
stress importance	cause injury	2
sell property	hold meeting	1
pay price	cut cost	2
consider matter	discuss issue	5
encourage child	leave company	1
shut door	follow road	2
produce effect	start work	2
set example	provide system	1
begin career	suffer loss	2
need treatment	use method	5
pose problem	face difficulty	3
ask man	send message	4
attend conference	require attention	2
read word	receive letter	3
collect information	provide datum	4
win match	reduce amount	2
pass time	cross line	3
hear word	remember name	3
leave house	buy home	3
achieve result	reach level	5
acquire skill	use test	4
lift hand	raise head	3
like people	increase number	1
satisfy demand	emphasise need	5
play game	join party	5
stretch arm	wave hand	4
share interest	express view	6
use knowledge	acquire skill	5
fight war	increase number	1
develop technique	use method	5
achieve end	reach level	5
remember name	pass time	2
pour tea	join party	2
address question	pose problem	4
receive letter	collect information	4
express view	suffer loss	2
start work	begin career	7
buy land	leave house	3
discuss issue	present problem	6
stress importance	emphasise need	6
sell property	buy home	6
help people	encourage child	4
reduce amount	cut cost	7
cause injury	raise head	1
shut door	provide datum	1
cross line	follow road	3
produce effect	consider matter	3
send message	hear word	4
need treatment	offer support	4
win battle	meet requirement	4
attend conference	hold meeting	5
win match	play game	5
provide system	use power	2
achieve result	lift hand	2
close eye	stretch arm	1
face difficulty	set example	3
like people	ask man	4
write book	read word	4
require attention	pay price	2
wave hand	leave company	1
drink water	use test	2
share interest	exercise influence	3
provide help	satisfy demand	5
use knowledge	provide system	3
fight war	win match	4
offer support	cross line	1
pour tea	emphasise need	1
write book	hear word	1
provide datum	reach level	1
follow road	set example	1
buy land	sell property	3
stress importance	cut cost	1
ask man	stretch arm	1
help people	discuss issue	3
raise head	address question	2
pay price	begin career	1
consider matter	pose problem	3
join party	leave company	2
shut door	close eye	2
increase number	reduce amount	2
send message	receive letter	3
win battle	play game	4
attend conference	share interest	2
read word	remember name	2
suffer loss	cause injury	2
use test	pass time	4
leave house	encourage child	1
present problem	face difficulty	3
achieve result	produce effect	6
acquire skill	buy home	1
like people	collect information	2
use power	exercise influence	7
hold meeting	lift hand	1
satisfy demand	meet requirement	6
require attention	need treatment	7
wave hand	start work	1
express view	achieve end	2
drink water	use method	1
provide help	develop technique	3
use knowledge	exercise influence	7
fight war	win battle	5
offer support	provide help	7
develop technique	use power	6
achieve end	close eye	3
meet requirement	help people	3
pour tea	drink water	4
write book	buy land	2
address question	present problem	6
stress importance	cause injury	3
sell property	hold meeting	4
pay price	cut cost	6
consider matter	discuss issue	6
encourage child	leave company	2
shut door	follow road	3
produce effect	start work	4
set example	provide system	3
begin career	suffer loss	3
need treatment	use method	3
pose problem	face difficulty	6
ask man	send message	3
attend conference	require attention	4
read word	receive letter	5
collect information	provide datum	7
win match	reduce amount	3
pass time	cross line	3
hear word	remember name	6
leave house	buy home	5
achieve result	reach level	6
acquire skill	use test	5
lift hand	raise head	3
like people	increase number	1
satisfy demand	emphasise need	6
play game	join party	3
stretch arm	wave hand	5
share interest	express view	6
use knowledge	provide system	2
fight war	win match	4
offer support	cross line	1
pour tea	emphasise need	1
satisfy demand	meet requirement	6
provide datum	reach level	1
follow road	set example	1
buy land	sell property	5
stress importance	cut cost	1
ask man	stretch arm	1
help people	discuss issue	1
raise head	address question	2
achieve result	produce effect	5
consider matter	pose problem	4
join party	leave company	1
shut door	close eye	1
increase number	reduce amount	4
send message	receive letter	4
win battle	play game	5
attend conference	share interest	2
read word	remember name	1
suffer loss	cause injury	4
use test	pass time	1
leave house	encourage child	1
present problem	face difficulty	3
pay price	begin career	1
acquire skill	buy home	1
like people	collect information	1
use power	exercise influence	6
hold meeting	lift hand	1
write book	hear word	2
require attention	need treatment	7
wave hand	start work	1
express view	achieve end	4
drink water	use method	1
provide help	develop technique	2
use knowledge	exercise influence	5
fight war	win battle	5
offer support	provide help	6
develop technique	use power	2
achieve end	close eye	2
meet requirement	help people	4
pour tea	drink water	2
write book	buy land	1
address question	present problem	5
stress importance	cause injury	1
sell property	hold meeting	1
achieve result	reach level	5
consider matter	discuss issue	5
encourage child	leave company	1
shut door	follow road	1
produce effect	start work	2
set example	provide system	2
begin career	suffer loss	2
need treatment	use method	3
pose problem	face difficulty	4
ask man	send message	5
attend conference	require attention	2
read word	receive letter	2
collect information	provide datum	2
win match	reduce amount	1
pass time	cross line	2
hear word	remember name	1
leave house	buy home	1
pay price	cut cost	2
acquire skill	use test	2
lift hand	raise head	2
like people	increase number	1
satisfy demand	emphasise need	2
play game	join party	4
stretch arm	wave hand	5
share interest	express view	7
use knowledge	provide system	1
fight war	win match	3
offer support	cross line	2
pour tea	emphasise need	2
write book	hear word	2
provide datum	reach level	2
follow road	set example	2
buy land	sell property	3
stress importance	cut cost	1
ask man	stretch arm	1
help people	discuss issue	4
raise head	address question	1
achieve result	produce effect	5
consider matter	pose problem	4
join party	leave company	3
shut door	close eye	1
increase number	reduce amount	3
send message	receive letter	2
win battle	play game	3
attend conference	share interest	3
read word	remember name	2
suffer loss	cause injury	2
use test	pass time	1
leave house	encourage child	1
present problem	face difficulty	2
pay price	begin career	2
acquire skill	buy home	2
like people	collect information	1
use power	exercise influence	5
hold meeting	lift hand	1
satisfy demand	meet requirement	6
require attention	need treatment	6
wave hand	start work	1
express view	achieve end	2
drink water	use method	2
provide help	develop technique	2
use knowledge	acquire skill	7
fight war	increase number	3
develop technique	use method	5
achieve end	reach level	6
remember name	pass time	3
pour tea	join party	2
address question	pose problem	6
receive letter	collect information	6
express view	suffer loss	3
start work	begin career	6
buy land	leave house	1
discuss issue	present problem	7
stress importance	emphasise need	7
sell property	buy home	4
help people	encourage child	6
reduce amount	cut cost	7
cause injury	raise head	2
shut door	provide datum	1
cross line	follow road	5
produce effect	consider matter	4
send message	hear word	5
need treatment	offer support	3
win battle	meet requirement	5
attend conference	hold meeting	6
win match	play game	4
provide system	use power	5
achieve result	lift hand	2
close eye	stretch arm	1
face difficulty	set example	3
like people	ask man	1
write book	read word	3
require attention	pay price	4
wave hand	leave company	1
drink water	use test	2
share interest	exercise influence	5
provide help	satisfy demand	5
