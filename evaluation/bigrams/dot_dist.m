function [ D ] = dot_dist( ZI, ZJ )

[M, ~] = size(ZJ);

D = zeros(M,1);

for i=1:1:M
   vv = ZJ(i,:);
   D(i) = dot(vv,ZI);
end

end

