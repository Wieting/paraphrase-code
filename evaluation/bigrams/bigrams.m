
function [accs] = bigrams(theta,We_orig,hiddenSize,words,dfile,type)

switch nargin
    case 5
        type = 'cosine';
end

accs = getAcc(theta, We_orig, hiddenSize, words, dfile, type);
end

function [acc] = getAcc(theta, We_orig, hiddenSize, words, dfile, type)

wordvects = We_orig';
wordvocab = words';
vocab_indices = (1:length(wordvocab))';
wordvocab_map = containers.Map(wordvocab, vocab_indices);

W1 = reshape(theta(1:hiddenSize*hiddenSize),hiddenSize,hiddenSize);
W2 = reshape(theta(hiddenSize*hiddenSize+1:2*hiddenSize*hiddenSize),hiddenSize,hiddenSize);
bw1 = reshape(theta(2*hiddenSize*hiddenSize+1:2*hiddenSize*hiddenSize+hiddenSize),hiddenSize,1);

%%%%%% Read in bigram similarity dataset.
fid3 = fopen(dfile, 'r', 'n', 'UTF-8');
bigramsimdata = textscan(fid3,'%s %s %f', 'delimiter','\t', 'HeaderLines', 1);
fclose(fid3);
%fprintf('Read in bigram similarity dataset \n');

%%%%%% Compute bigram similarity metrics.

bigrams = [bigramsimdata{1}; bigramsimdata{2}];
% Get bigram embeddings for the bigrams in the bigram similarity dataset.
bigram_vects = embed_phrases(wordvocab_map, wordvects, bigrams, 0,1);

m=wordvocab_map;
v=wordvects;
w=bigrams;

% Tokenize the phrases.
ss = repmat({'%s'},size(w, 1),1);
toks = cellfun(@strread, w, ss, 'UniformOutput', false);

ms = repmat({m},size(w, 1),1);
print_backoff_flags = repmat({0}, size(w,1),1);
F = cellfun(@lookup_softmatch, ms, toks, print_backoff_flags, 'UniformOutput', false);

nn = size(F,1);
num_words = size(F{1},2);

E = zeros(nn, hiddenSize);
for i=1:nn
    v1=v(F{i}(1),:)';
    v2=v(F{i}(2),:)';
    E(i,:) = sigmoid(W1*v1+W2*v2+bw1);
    %E(i,:) = v1+v2;
end

bigram_vects=E;
acc = compute_sim_corr(bigram_vects, bigrams, bigramsimdata{1}, bigramsimdata{2}, bigramsimdata{3},type);

end