function [acc] = wordsim(We_orig, words)

if(exist('../bigrams/'))
    addpath('../bigrams/');
end
acc= [];
wordvects = We_orig';
wordvocab = words';
vocab_indices = (1:length(wordvocab))';
wordvocab_map = containers.Map(wordvocab, vocab_indices);


%%%%%% Read in word similarity dataset.
fid3 = fopen('wordsim353.txt', 'r', 'n', 'UTF-8');
wordsimdata = textscan(fid3,'%s %s %f', 'delimiter','\t', 'HeaderLines', 1);
fclose(fid3);
%fprintf('Read in full word similarity dataset\n');

fid3 = fopen('wordsim-sim.txt', 'r', 'n', 'UTF-8');
simdata = textscan(fid3,'%s %s %f', 'delimiter','\t', 'HeaderLines', 1);
fclose(fid3);
%fprintf('Read in sim portion of word similarity dataset\n');

fid3 = fopen('wordsim-rel.txt', 'r', 'n', 'UTF-8');
reldata = textscan(fid3,'%s %s %f', 'delimiter','\t', 'HeaderLines', 1);
fclose(fid3);
%fprintf('Read in rel portion of word similarity dataset\n');

fid = fopen('SimLex-999.txt','r');
sim999  = textscan(fid,'%s%s%*s%f%*s%*s%*s%*s%*s%*s', 'delimiter','\t', 'HeaderLines', 1);
fclose(fid);

%%%%%% Compute word similarity metrics.
wordsimwords = [wordsimdata{1}; wordsimdata{2}];
% Look up embeddings for the words in the word similarity dataset.
[relevant_inds, num_unk] = lookup_softmatch(wordvocab_map, wordsimwords, 1);
relevant_vects = wordvects(relevant_inds,:);
fprintf('Num unk wordsim = %d\n', num_unk);
thecorr = compute_sim_corr(relevant_vects, wordsimwords, wordsimdata{1}, wordsimdata{2}, wordsimdata{3});
fprintf('wordsim corr=%f\n', thecorr);
acc(end+1) = thecorr;

simwords = [simdata{1}; simdata{2}];
[relevant_inds, num_unk] = lookup_softmatch(wordvocab_map, simwords, 1);
relevant_vects = wordvects(relevant_inds,:);
fprintf('Num unk sim = %d\n', num_unk);
thecorr = compute_sim_corr(relevant_vects, simwords, simdata{1}, simdata{2}, simdata{3});
fprintf('sim corr=%f\n',  thecorr);
acc(end+1) = thecorr;

relwords = [reldata{1}; reldata{2}];
[relevant_inds, num_unk] = lookup_softmatch(wordvocab_map, relwords, 1);
relevant_vects = wordvects(relevant_inds,:);
fprintf('Num unk rel = %d\n', num_unk);
thecorr = compute_sim_corr(relevant_vects, relwords, reldata{1}, reldata{2}, reldata{3});
fprintf('rel corr=%f\n', thecorr);
acc(end+1) = thecorr;

sim999words = [sim999{1}; sim999{2}];
% Look up embeddings for the words in the word similarity dataset.
[relevant_inds, num_unk] = lookup_softmatch(wordvocab_map, sim999words, 1);
relevant_vects = wordvects(relevant_inds,:);
fprintf('Num unk wordsim = %d\n', num_unk);
thecorr = compute_sim_corr(relevant_vects, sim999words, sim999{1}, sim999{2}, sim999{3});
fprintf('simlex corr=%f\n', thecorr);
acc(end+1) = thecorr;

end
