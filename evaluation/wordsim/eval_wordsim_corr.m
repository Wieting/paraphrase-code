addpath('../../paraphrase_RNN_words/code/core/');
load('../../paraphrase_RNN_words/code/test/data/skipwiki25.mat')

fid = fopen('log_wordsim_skipwiki25.txt','w');
count = 0;
files=dir('../../paraphrase_RNN_words/params/');
for k=1:length(files)
    fname = files(k).name
    
    if(length(fname) <= 2)
        continue;
    end
    
    load(strcat('../../paraphrase_RNN_words/params/',fname));
    acc = wordsim(We_orig,words);
    fprintf(fid,'%s....%d....%d....%d....%d\n',fname,acc(1),acc(2),acc(3),acc(4));
    count = count + 1;
    if(count > 20)
        %break;
    end
end

fclose(fid);