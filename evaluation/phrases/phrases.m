function [accs] = phrases(theta,We_orig,hiddenSize,words)

c1=phrases_nn(theta,We_orig,hiddenSize,words,'../../../Paraphrase_Project_data/core_data/d200_scored.txt.mat');
c2=phrases_nn(theta,We_orig,hiddenSize,words,'../../evaluation/semeval/SICK_test_annotated.txt.mat');
%c2=0;

accs=[c1 c2];
end
