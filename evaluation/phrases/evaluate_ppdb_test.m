addpath('../../single_W_code/code/core/')
load('../../core_data/skipwiki25.mat');
load('../../models/skipwiki25SL999_0_0.0001_100_phrase_60k.params16.mat');
hiddenSize=25;

[pred, gold]=phrases_nn2(theta,We_orig,hiddenSize,words,'../../core_data/ppdb_test.txt.mat');

fid = fopen('pred_test.txt','wt');
fprintf(fid,'%f\n',pred);
fclose(fid);

fid = fopen('gold_test.txt','wt');
fprintf(fid,'%f\n',gold);
fclose(fid);

[pred, gold]=phrases_nn2(theta,We_orig,hiddenSize,words,'../../core_data/ppdb_dev.txt.mat');

fid = fopen('pred_dev.txt','wt');
fprintf(fid,'%f\n',pred);
fclose(fid);

fid = fopen('gold_dev.txt','wt');
fprintf(fid,'%f\n',gold);
fclose(fid);