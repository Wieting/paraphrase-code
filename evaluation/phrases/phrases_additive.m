function [c]=phrases_additive(We_orig, dfile)

load(dfile);
train_data = [train_data valid_data test_data];

x = [];
y = [];

for i=1:1:length(train_data)
    [~,sl1] = size(train_data{i}{1}.nums);
    [~,sl2] = size(train_data{i}{2}.nums);
    train_data{i}{1}=ffTree(train_data{i}{1}, We_orig);
    train_data{i}{2}=ffTree(train_data{i}{2}, We_orig);
    g1 = sum(train_data{i}{1}.nodeFeaturesforward,2)/sl1;
    g2 = sum(train_data{i}{2}.nodeFeaturesforward,2)/sl2;
    d = dot(g1,g2)/(norm(g1)*norm(g2));
    x = [x d];
    y = [y train_data{i}{3}];
end

[c,~] = corr(x',y','Type','Spearman');

end


function [t] = ffTree(t, We_orig)
    [n, ~] = size(We_orig);
    [~,sl] = size(t.nums);
    words_indexed = t.nums;
    words_embedded = We_orig(:, words_indexed);
    t.nodeFeaturesforward = zeros(n, sl);
    t.nodeFeaturesforward(:,1:sl) = words_embedded;
end