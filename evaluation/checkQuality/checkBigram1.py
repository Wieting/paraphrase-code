import ntpath
from glob import glob

files = '../../models/*.mat'

lines=[]
for file in glob(files):
    lines.append(file)

lambda1 = [0, 0.1, 0.01, 0.001]
lambda2 = [100, 10, 1, 0.1, 0.01, 0.001, 0.0001, 0.00001]
#lambda2 = [0.01, 0.001, 0.0001, 0.00001, 0.000001]
batchsize = [100,250,500,1000,2000]
types=['adj-noun','verb-noun','noun-noun']

an_name = 'adj-noun-xl'
an_data = '../../../Paraphrase_Project_data/bigram_data/adj-noun-base-xl.txt.mat'

nn_name = 'noun-noun-xl'
nn_data = '../../../Paraphrase_Project_data/bigram_data/noun-noun-base-xl.txt.mat'

vn_name = 'verb-noun-xl'
vn_data = '../../../Paraphrase_Project_data/bigram_data/verb-noun-base-xl.txt.mat'

lis=[]
for i in lambda1:
    for j in lambda2:
        for k in batchsize:
            for l in types:
                newl=str(i)+'_'+str(j)+'_'+str(k)+'_'+l
                lis.append(newl)


for i in lines:
    ff=ntpath.basename(i.strip())
    f=ff.split('_')
    param1=f[1]
    param2=f[2]
    param3=f[3]
    type=f[4].split('.')[0]
    if('params80' in ff):
        newl=param1+'_'+param2+'_'+param3+'_'+type
        newl = newl.replace('-xl','')
        #print newl, newl in lis
        lis.remove(newl)

#for i in lis:
#    print i

for i in lis:
    i=i.split('_')
    p1=float(i[0])
    p2=float(i[1])
    p3=float(i[2])
    n=i[3]
    df=''
    f=''
    if('adj-noun' in n):
        df=an_data
        f=an_name
    if('verb-noun' in n):
        df=vn_data
        f=vn_name
    if('noun-noun' in n):
        df=nn_data
        f=nn_name
    cmd = str(p1)+", "+str(p2)+", 1.0, "+str(p3)+", '"+df+"', '"+f+"', 'config1.m'"
    cmd = "/opt/matlab-r2013a/bin/matlab -nodisplay -nodesktop -nojvm -nosplash -r \"train_single("+cmd+");quit\""
    print cmd

#/opt/matlab-r2013a/bin/matlab -nodisplay -nodesktop -nojvm -nosplash -r "train_single(0, 0.0001, 1.0, 1000, '../../../Paraphrase_Project_data/bigram_data/adj-noun-base-xl.txt.mat', 'adj-noun-xl', 'config1.m');quit"
        