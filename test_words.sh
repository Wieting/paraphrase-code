#test word code
echo "Testing word code"
cd word_code/training/
alias matlab=/Applications/MATLAB_R2014a.app/bin/matlab
matlab -nodisplay -nodesktop -nojvm -nosplash -r "train_single(0, 0.0001, 0.2, 'words-xl', '../../../Paraphrase_Project_data/word_data_done/words.txt', 100, 'config1.m');quit"

cd ../..
