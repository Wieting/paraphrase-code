function [] = testParamsPhrases(fname,theta,We,hiddenSize,words)
    [~,fname,~] = fileparts(fname);
    acc = phrases(theta,We,hiddenSize,words);
    fprintf('phrases.......%s.......%d.......%d\n',fname,acc(1),acc(2));
end
