# sort by difference and then score with overlap, length, distortion
def getLengthScore(s1,s2):
    score = lengthscore(s1,s2)
    return "length: "+str(score)

#def lengthscore(s1,s2):
#    s1=s1.split()
#    s2=s2.split()
#    l1 = len(s1)
#    l2 = len(s2)
#    return abs(l1-l2)

def lengthscore(s1,s2):
    s1=s1.split()
    s2=s2.split()
    l1 = len(s1)
    l2 = len(s2)
    s = l1/float(l2)
    if(l2 < l1):
        s = l2 / float(l1)
    return s

def overlapscore2(s1,s2,d):
    s1=s1.split()
    s2=s2.split()
    ct = 0
    l = len(s1)
    arrs=s1
    arrl=s2
    if len(s2) < l:
        l = len(s2)
        arrs=s2
        arrl=s1
    for i in arrs:
        for j in arrl:
            if i==j:
                ct += 1
                break
            if i in d:
                t = d[i]
                if j in t:
                    ct += 1
                    break
    return float(ct)/l

def alignment(s1,s2,d):
    al=""
    s1=s1.split()
    s2=s2.split()
    l = len(s1)
    arrs=s1
    arrl=s2
    if len(s2) < l:
        l = len(s2)
        arrs=s2
        arrl=s1
    for i in arrs:
        b = False
        for j in arrl:
            if i==j:
                al = al + i+":"+j+" "
                b=True
                break
            if i in d:
                t = d[i]
                if j in t:
                    al = al + i+":"+j+" "
                    b = True
                    break
        if(not b):
            al = al + i+":NA "
    return al

def overlapscore(s1,s2,d):
    s1=s1.split()
    s2=s2.split()
    ct = 0
    l = len(s1)
    arrs=s1
    arrl=s2
    if len(s2) < l:
        l = len(s2)
        arrs=s2
        arrl=s1
    for i in arrs:
        for j in arrl:
            if i==j:
                ct += 1
                break
            if i in d:
                t = d[i]
                if j in t:
                    ct += 1
                    break
    score = float(ct)/l
    return "overlap: "+str(score)


def getdistortionscore(s1,s2,d):
    score = overlapscore2(s1,s2,d)
    if(score < 0.01):
        return "distortion: NA"
    s1=s1.split()
    s2=s2.split()
    l = len(s1)
    arrs=s1
    arrl=s2
    if len(s2) < l:
        l = len(s2)
        arrs=s2
        arrl=s1
    ct = 0
    tot = 0
    for (ni,i) in enumerate(arrs):
        for (nj,j) in enumerate(arrl):
            if i==j:
                ct += 1
                tot += abs(ni-nj)
                break
            if i in d:
                t = d[i]
                if j in t:
                    ct += 1
                    tot += abs(ni-nj)
                    break
    score = float(tot)/ct
    score = score/(len(s1)+len(s2))
    return "distortion: "+str(score)

class phrase:
    def __init__(self,line,diff):
        d = line.split()[1]
        g = line.split()[4]
        s1 = line.split("|||")[1].strip()
        s2 = line.split("|||")[2].split('\t')[0].strip()
        add_pred=float(line.split()[5])
        rnn_pred=float(line.split("|||")[2].split("\t")[1].split()[3])
        self.line = d+"\t"+g+"\t"+str(rnn_pred)+"\t"+str(add_pred)+"\t"+s1+"\t"+s2
        self.diff = diff
        self.ls = ""
        self.os = ""
        self.ds = ""
        self.al = ""

d={}
f2=open('/Users/Wieting/Desktop/Paraphrase_Project_data/word_data_done/words.txt','r')
lines2=f2.readlines()

for li in lines2:
    li=li.split()
    w1 = li[0]
    w2 = li[1]
    if w1 not in d:
        t=[]
        t.append(w2)
        d[w1]=t
    else:
        t = d[w1]
        t.append(w2)
        d[w1]=t
    if w2 not in d:
        t=[]
        t.append(w1)
        d[w2]=t
    else:
        t = d[w2]
        t.append(w1)
        d[w2]=t

f=open('output.txt','r')
lines=f.readlines()

lis=[]
for li in lines:
    i=li.split('\t')
    if(float(i[2].split()[2]) <2 or float(i[2].split()[2]) > 4):
        diff = abs(float(i[1]))
        p = phrase(li,diff)
        s1 = li.split("|||")[1].strip()
        s2 = li.split("|||")[2].split('\t')[0].strip()
        p.ls=getLengthScore(s1,s2)
        p.os=overlapscore(s1,s2,d)
        p.ds=getdistortionscore(s1,s2,d)
        p.al=alignment(s1,s2,d)
        lis.append(p)

newlis = sorted(lis, key=lambda x: x.diff, reverse=True)

neg_low_length = open('qual_results/result_neg_low_length','w')
neg_high_length = open('qual_results/result_neg_high_length','w')
neg_low_overlap = open('qual_results/result_neg_low_overlap','w')
neg_high_overlap = open('qual_results/result_neg_high_overlap','w')

pos_low_length = open('qual_results/result_pos_low_length','w')
pos_high_length = open('qual_results/result_pos_high_length','w')
pos_low_overlap = open('qual_results/result_pos_low_overlap','w')
pos_high_overlap = open('qual_results/result_pos_high_overlap','w')
for i in newlis:
    #if("NA" not in i.ds and float(i.line.split()[0]) < 0 and float(i.line.split()[1]) > 3):
    ls = float(i.ls.split(":")[1])
    os = float(i.os.split(":")[1])
    g = float(i.line.split()[1])
    line = i.line
    print line + "\t" + i.ls + "\t" + "\t" + i.os
    if ( g < 2 and ls <= 0.6):
        neg_low_length.write(line + "\t" + i.ls + "\t" + "\t" + i.os + "\n")
    if ( g < 2 and ls >= 0.8):
        neg_high_length.write(line + "\t" + i.ls + "\t" + "\t" + i.os + "\n")
    if ( g < 2 and os <= 0.34):
        neg_low_overlap.write(line + "\t" + i.ls + "\t" + "\t" + i.os + "\n")
    if ( g < 2 and os >= 0.66):
        neg_high_overlap.write(line + "\t" + i.ls + "\t" + "\t" + i.os + "\n")
    if ( g > 4 and ls <= 0.6):
        pos_low_length.write(line + "\t" + i.ls + "\t" + "\t" + i.os + "\n")
    if ( g > 4 and ls >= 0.8):
        pos_high_length.write(line + "\t" + i.ls + "\t" + "\t" + i.os + "\n")
    if ( g > 4 and os <= 0.34):
        pos_low_overlap.write(line + "\t" + i.ls + "\t" + "\t" + i.os + "\n")
    if ( g > 4 and os >= 0.66):
        pos_high_overlap.write(line + "\t" + i.ls + "\t" + "\t" + i.os + "\n")
