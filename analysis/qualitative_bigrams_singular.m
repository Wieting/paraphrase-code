function [ ] = qualitative_bigrams_singular(base,d1,addornn1,dataset,f1)

fid1 = fopen(f1,'w');

if(nargin==4)
    fid1=1;
end

load(base);

[x1,y,data] = bigrams2(d1,25,words,dataset,addornn1);

x1=(x1-min(x1)).*4/(max(x1)-min(x1))+1;
[v,i]=sort(abs(x1-y),'descend');
c1=corr(x1,y,'Type','Spearman');
dispTopNwords(v,i,data,words,x1,length(x1),c1,fid1);
end

function [] = dispTopNwords(v,i,data,words,x,N,c,fid)

p1=data{1};
p2=data{2};
dd=data{3};

l=length(i);
for j=1:1:N
   fprintf(fid,'%i %f %f %f ||| ',j, v(l-j+1), dd(i(l-j+1)), x(i(l-j+1)));
   fprintf(fid,'%s ||| %s\n',p1{i(l-j+1)},p2{i(l-j+1)});
end
fprintf(fid,'%f\n',c);

end

function [x,y,data] = bigrams2(d, hiddenSize, words, dfile, type)
load(d);
wordvects = We_orig';
wordvocab = words';
vocab_indices = (1:length(wordvocab))';
wordvocab_map = containers.Map(wordvocab, vocab_indices);

if(exist('theta'))
W1 = reshape(theta(1:hiddenSize*hiddenSize),hiddenSize,hiddenSize);
W2 = reshape(theta(hiddenSize*hiddenSize+1:2*hiddenSize*hiddenSize),hiddenSize,hiddenSize);
bw1 = reshape(theta(2*hiddenSize*hiddenSize+1:2*hiddenSize*hiddenSize+hiddenSize),hiddenSize,1);
end

fid3 = fopen(dfile, 'r', 'n', 'UTF-8');
bigramsimdata = textscan(fid3,'%s %s %f', 'delimiter','\t', 'HeaderLines', 1);
fclose(fid3);

bigrams = [bigramsimdata{1}; bigramsimdata{2}];
data = {bigramsimdata{1} bigramsimdata{2}  bigramsimdata{3}};
bigram_vects = embed_phrases(wordvocab_map, wordvects, bigrams, 0,1);

m=wordvocab_map;
v=wordvects;
w=bigrams;

ss = repmat({'%s'},size(w, 1),1);
toks = cellfun(@strread, w, ss, 'UniformOutput', false);

ms = repmat({m},size(w, 1),1);
print_backoff_flags = repmat({0}, size(w,1),1);
F = cellfun(@lookup_softmatch, ms, toks, print_backoff_flags, 'UniformOutput', false);

nn = size(F,1);

E = zeros(nn, hiddenSize);
for i=1:nn
    v1=v(F{i}(1),:)';
    v2=v(F{i}(2),:)';
    if(type==0)
        E(i,:) = sigmoid(W1*v1+W2*v2+bw1);
    else
        E(i,:) = v1+v2;
    end
end

bigram_vects=E;
[x,y] = compute_sim_corr2(bigram_vects, bigrams, bigramsimdata{1}, bigramsimdata{2}, bigramsimdata{3});

end

function [x, y]=compute_sim_corr2(ven, vocaben, gold_corr_en1, gold_corr_en2, goldsims)

vocab_indices = (1:length(vocaben))';
vocab_map = containers.Map(vocaben, vocab_indices);

relevantinds1 = lookup_softmatch(vocab_map, gold_corr_en1);
relevantinds2 = lookup_softmatch(vocab_map, gold_corr_en2);

D = pdist2(ven, ven, 'cosine');

ddd = length(relevantinds1);
sims = zeros(ddd,1);

for i = 1:ddd,
    sims(i) = -D(relevantinds1(i), relevantinds2(i))+1;
end

x=sims;
y=goldsims;

end