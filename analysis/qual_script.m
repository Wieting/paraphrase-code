skip25='params/skipwiki25.mat';
skipsl999='params/skipwiki25_1e-05_250_words-xl.params63.mat.ws353sl99.mat';
skipws353='params/skipwiki25_0.0001_100_words-xl.params70.mat.ws353.mat';
skipsl999RNN='params/skipwiki25SL999_0.001_0.0001_100_phrase_60k.params15.ppdbsl999.mat';
skip25RNN='params/skipwiki25_0.01_1e-05_1000_phrase_60k.params5.ppdb.mat';
ppdb1k='../core_data/ppdb_test.txt.mat';
ppdb3k='../core_data/3000data.mat';

skipsl999RNNnn='params/skipwiki25SL999_0.001_0.001_100_noun-noun-xl.params25.nn.mat';
skipsl999RNNvn='params/skipwiki25SL999_0.1_10_250_noun-noun-xl.params5.vn.mat';
skipsl999RNNjn='params/skipwiki25SL999_0.01_0.1_2000_adj-noun-xl.params13.jn.mat';

kj_nn='../evaluation/bigrams/data/kj_nn.txt';
kj_vn='../evaluation/bigrams/data/kj_vn.txt';
kj_jn='../evaluation/bigrams/data/kj_an.txt';

addpath('../evaluation/bigrams/');
addpath('../evaluation/wordsim/');
addpath('../evaluation/semeval/');
addpath('../evaluation/phrases/');
addpath('../single_W_code/code/core/');

%phrase_corr(skip25,skip25,1,skipws353,1,ppdb3k);
%phrase_corr(skip25,skip25,1,skipsl999,1,ppdb3k);

%phrase_corr(skip25,skipsl999,1,skipsl999RNN,0,ppdb1k);

%phrase_corr(skip25,skip25,1,skip25RNN,0,ppdb1k);
%phrase_corr(skip25,skip25,1,skipws353,1,ppdb1k);
%phrase_corr(skip25,skip25,1,skipsl999,1,ppdb1k);
%phrase_corr(skip25,skip25,1,skipsl999RNN,0,ppdb1k);

%qualitative(skip25,skip25,1,skipsl999RNN,0,ppdb1k);
qualitative_bigrams_singular(skip25,skipsl999,1,kj_nn,'qual_results/25_sl999_nn.txt');
qualitative_bigrams_singular(skip25,skipsl999,1,kj_vn,'qual_results/25_sl999_vn.txt');
qualitative_bigrams_singular(skip25,skipsl999,1,kj_jn,'qual_results/25_sl999_jn.txt');

qualitative_bigrams_singular(skip25,skipsl999RNNnn,0,kj_nn,'qual_results/25_sl999_rnn_nn.txt');
qualitative_bigrams_singular(skip25,skipsl999RNNvn,0,kj_vn,'qual_results/25_sl999_rnn_vn.txt');
qualitative_bigrams_singular(skip25,skipsl999RNNjn,0,kj_jn,'qual_results/25_sl999_rnn_jn.txt');

qualitative_phrases_singular(skip25,skipsl999,1,ppdb1k,'qual_results/25_sl999_ppdb.txt');
qualitative_phrases_singular(skip25,skipsl999RNN,0,ppdb1k,'qual_results/25_sl999_rnn_ppdb.txt');