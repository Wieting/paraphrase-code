
def getLengthDifficulty(lines,neg):
    error_0=0
    error_1=0
    error_2=0
    error_3=1
    t_0=0
    t_1=0
    t_2=0
    t_3=1
    for i in lines :
        #print i.strip()
        if "|||" not in i:
            continue
        l=i.split()
        error = float(l[1])
        score = float(l[2])
        if(score >= 2 and score < 4):
            continue
        if(neg > 0 and score < 3):
            continue
        if(neg < 0 and score > 3):
            continue
        s1 = i.split("|||")[1].strip()
        s2 = i.split("|||")[2].strip()
        score = lengthscore(s1,s2)
        if (score <= .6):
            error_0 += error
            t_0 += 1
        elif (score >= .8):
            error_1 += error
            t_1 += 1
        else:
            error_3 += error
            t_3 += 1
    print round(error_0/t_0,2), round(error_1/t_1,2),  round(error_3/t_3,2)
    print t_0, t_1, t_2, t_3
    return round(error_0/t_0,2), round(error_1/t_1,2),  round(error_3/t_3,2)


def lengthscore(s1,s2):
    s1=s1.split()
    s2=s2.split()
    l1 = len(s1)
    l2 = len(s2)
    s = l1/float(l2)
    if(l2 < l1):
        s = l2 / float(l1)
    return s


f = open('qual_results/25_sl999_ppdb.txt')
lines1 = f.readlines()

f = open('qual_results/25_sl999_rnn_ppdb.txt')
lines2 = f.readlines()

a1,a2,a3=getLengthDifficulty(lines1,1)
r1,r2,r3=getLengthDifficulty(lines2,1)
print round((r1-a1)/a1*100,2),round((r2-a2)/a2*100,2),round((r3-a3)/a3*100,2)
print ""
a1,a2,a3=getLengthDifficulty(lines1,-1)
r1,r2,r3=getLengthDifficulty(lines2,-1)
print round((r1-a1)/a1*100,2),round((r2-a2)/a2*100,2),round((r3-a3)/a3*100,2)
print ""
a1,a2,a3=getLengthDifficulty(lines1,0)
r1,r2,r3=getLengthDifficulty(lines2,0)
print round((r1-a1)/a1*100,2),round((r2-a2)/a2*100,2),round((r3-a3)/a3*100,2)
"""
print "ADD-POS 0, 1, 2, 3+"
getLengthDifficulty(lines1,1)
print "RNN-POS 0, 1, 2, 3+"
getLengthDifficulty(lines2,1)
print ""
print "ADD-NEG 0, 1, 2, 3+"
getLengthDifficulty(lines1,-1)
print "RNN-NEG 0, 1, 2, 3+"
getLengthDifficulty(lines2,-1)
print ""
print "ADD-ALL 0, 1, 2, 3+"
getLengthDifficulty(lines1,0)
print "RNN-ALL 0, 1, 2, 3+"
getLengthDifficulty(lines2,0)
"""

