function [] = sl999_corr_output(base,d1,d2,fid,name)

load(base);
load(d1);

[n1, ~] = size(We_orig);
load(d2);
[n2, ~] = size(We_orig);

[x1,y] = wordsim2(d1,n1,words);

load(d2);
[x2,~] = wordsim2(d2,n2,words);
c1=corr(x1,y,'Type','Spearman');
c2=corr(x2,y,'Type','Spearman');
c3=corr(x1,x2,'Type','Spearman');
l=length(x1);
fprintf(fid,'%s\t%f\t%f\t%f\t%f\n',sprintf(name),c1,c2,c3,l);

end

function [x,y] = wordsim2(d, hiddenSize, words)
load(d);
wordvects = We_orig';
wordvocab = words';
vocab_indices = (1:length(wordvocab))';
wordvocab_map = containers.Map(wordvocab, vocab_indices);

fid = fopen('SimLex-999.txt','r');
sim999  = textscan(fid,'%s%s%*s%f%*s%*s%*s%*s%*s%*s', 'delimiter','\t', 'HeaderLines', 1);
fclose(fid);

sim999words = [sim999{1}; sim999{2}];
% Look up embeddings for the words in the word similarity dataset.
[relevant_inds, num_unk] = lookup_softmatch(wordvocab_map, sim999words, 1);
relevant_vects = wordvects(relevant_inds,:);
fprintf('Num unk wordsim = %d\n', num_unk);
[x,y] = compute_sim_corr2(relevant_vects, sim999words, sim999{1}, sim999{2}, sim999{3});

end

function [x, y]=compute_sim_corr2(ven, vocaben, gold_corr_en1, gold_corr_en2, goldsims)

vocab_indices = (1:length(vocaben))';
vocab_map = containers.Map(vocaben, vocab_indices);

relevantinds1 = lookup_softmatch(vocab_map, gold_corr_en1);
relevantinds2 = lookup_softmatch(vocab_map, gold_corr_en2);

D = pdist2(ven, ven, 'cosine');

ddd = length(relevantinds1);
sims = zeros(ddd,1);

for i = 1:ddd,
sims(i) = -D(relevantinds1(i), relevantinds2(i))+1;
end

x=sims;
y=goldsims;

end
