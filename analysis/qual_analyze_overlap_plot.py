import matplotlib
import pylab

def getLabelDifficulty(lines):
    error_ones=0
    error_twos=0
    error_threes=0
    error_fours=0
    ones=0
    twos=0
    threes=0
    fours=0
    for i in lines :
        #print i.strip()
        if "|||" not in i:
            continue
        l=i.split()
        error = float(l[1])
        score = float(l[2])
        if (score < 2):
            error_ones += error
            ones += 1
        elif (score < 3):
            error_twos += error
            twos += 1
        elif (score < 4):
            error_threes += error
            threes += 1
        else:
            error_fours += error
            fours += 1
#print error_ones/ones, error_twos/twos, error_threes/threes, error_fours/fours
    print round(error_ones/ones,2), round(error_twos/twos,2), round(error_threes/threes,2), round(error_fours/fours,2)
    print ones, twos, threes, fours


def getOverlapDifficulty(lines,d,neg):
    x=[]
    y=[]
    for i in lines :
        #print i.strip()
        if "|||" not in i:
            continue
        l=i.split()
        error = float(l[1])
        score = float(l[2])
        if(score > 1.5 and score < 4.5):
            continue
        if(neg > 0 and score < 3):
            continue
        if(neg < 0 and score > 3):
            continue
        s1 = i.split("|||")[1].strip()
        s2 = i.split("|||")[2].strip()
        y.append(error)
        score = overlapscore(s1,s2,d)
        x.append(score)
    return x,y

def overlapscore(s1,s2,d):
    s1=s1.split()
    s2=s2.split()
    ct = 0
    l = len(s1)
    arrs=s1
    arrl=s2
    if len(s2) < l:
        l = len(s2)
        arrs=s2
        arrl=s1
    for i in arrs:
        for j in arrl:
            if i==j:
                ct += 1
                break
            if i in d:
                t = d[i]
                if j in t:
                    ct += 1
                    break
    return float(ct)/l

#make dictionary
f=open('output.txt','r')
lines=f.readlines()

d={}
f2=open('/Users/Wieting/Desktop/Paraphrase_Project_data/word_data_done/words.txt','r')
lines2=f2.readlines()

for li in lines2:
    li=li.split()
    w1 = li[0]
    w2 = li[1]
    if w1 not in d:
        t=[]
        t.append(w2)
        d[w1]=t
    else:
        t = d[w1]
        t.append(w2)
        d[w1]=t
    if w2 not in d:
        t=[]
        t.append(w1)
        d[w2]=t
    else:
        t = d[w2]
        t.append(w1)
        d[w2]=t


f = open('qual_results/25_sl999_ppdb.txt')
lines1 = f.readlines()

f = open('qual_results/25_sl999_rnn_ppdb.txt')
lines2 = f.readlines()

lines3=[]
for i in lines1:
    if "|||" not in i:
        continue
    ss=i.split("|||")[1]+i.split("|||")[2]
    for j in lines2:
        if "|||" not in j:
            continue
        tt = j.split("|||")[1]+j.split("|||")[2]
        if ss == tt:
            lines3.append(j)

x1,y1=getOverlapDifficulty(lines1,d,1)
x2,y2=getOverlapDifficulty(lines3,d,1)
y=[j - i for i, j in zip(y1, y2)]

tot=0
for i in y:
    tot += i
print tot

xx=[]
yy=[]
done=[]

for (n,i) in enumerate(x1):
    if i in done:
        continue
    tot=0
    ct=0
    for (nj,j) in enumerate(x1):
        if j==i:
            tot += y[nj]
            ct += 1
    done.append(i)
    xx.append(i)
    yy.append(tot/ct)
    print tot, ct, i, tot/ct

print y
print xx
print yy

matplotlib.pyplot.scatter(xx,yy)
matplotlib.pyplot.show()
