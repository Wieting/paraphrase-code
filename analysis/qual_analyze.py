
class Example:
    def __init__(self,s,v):
        self.s=s
        self.v=v


f = open('qual_results/25_sl999_ppdb.txt')
lines1 = f.readlines()

f = open('qual_results/25_sl999_rnn_ppdb.txt')
lines2 = f.readlines()

lis=[]

for li in lines1:
    if '|||' not in li:
        continue
    i=li.split('|||')
    d = float(i[0].split()[1])
    n = i[1]+i[2]
    for lj in lines2:
        if '|||' not in lj:
            continue
        j=lj.split('|||')
        dd = float(j[0].split()[1])
        nn = j[1]+j[2]
        if(nn==n):
            s=li.strip()+"\t"+lj.strip()
            v=d-dd
            c = Example(s,v)
            lis.append(c)

lis = sorted(lis, key=lambda x: x.v, reverse=True)

for (n,i) in enumerate(lis):
    print str(n)+'\t'+str(i.v)+'\t'+i.s