function [x,y] = phrase_additive(We_orig, dfile, type, comp)

switch nargin
    case 2
        type = 'cosine';
        comp = 'avg';
    case 3
        comp = 'avg';
end

load(dfile);
train_data = [train_data valid_data test_data];

x = [];
y = [];

for i=1:1:length(train_data)
    [~,sl1] = size(train_data{i}{1}.nums);
    [~,sl2] = size(train_data{i}{2}.nums);
    train_data{i}{1}=ffTree(train_data{i}{1}, We_orig);
    train_data{i}{2}=ffTree(train_data{i}{2}, We_orig);
    if(strcmp(comp,'avg')==1)
        g1 = sum(train_data{i}{1}.nodeFeaturesforward,2)/sl1;
        g2 = sum(train_data{i}{2}.nodeFeaturesforward,2)/sl2;
    else
        g1 = sum(train_data{i}{1}.nodeFeaturesforward,2);
        g2 = sum(train_data{i}{2}.nodeFeaturesforward,2);
    end
    if(strcmp(type,'cosine')==1)
        d = dot(g1,g2)/(norm(g1)*norm(g2));
    else
       d = dot(g1,g2); 
    end
    x = [x d];
    y = [y train_data{i}{3}];
end

end

function [t] = ffTree(t, We_orig)
    [n, ~] = size(We_orig);
    [~,sl] = size(t.nums);
    words_indexed = t.nums;
    words_embedded = We_orig(:, words_indexed);
    t.nodeFeaturesforward = zeros(n, sl);
    t.nodeFeaturesforward(:,1:sl) = words_embedded;
end