function [  ] = qualitative(base,d1,addornn1,d2,addornn2,dataset)

[x1,x2,y] = getScores(base,d1,addornn1,d2,addornn2,dataset);

x1=x1.*4+1;
x2=x2.*4+1;
load(base);
load(dataset);
train_data = [train_data valid_data test_data];
[v,i]=sort(abs(x1-y),'descend');
dispTopNwords(v,i,train_data,words,x1,len(train_data));
fprintf('\n');
%dispBottomNwords(v,i,train_data,words,x1,10);
[v,i]=sort(abs(x2-y),'descend');
fprintf('\n');
dispTopNwords(v,i,train_data,words,x2,len(train_data));
fprintf('\n');
%dispBottomNwords(v,i,train_data,words,x2,500);
c1=corr(x1',y','Type','Spearman')
c2=corr(x2',y','Type','Spearman')
end

function [] = dispTopNwords(v,i,train_data,words,x,N)

l=length(i);

for j=1:1:N
   t=train_data{i(l-j+1)};
   fprintf('%i %f %f %f ||| ',j, v(l-j+1), t{3}, x(i(l-j+1)))
   fprintf('%s ||| %s\n',strjoin(words(t{1}.nums)),strjoin(words(t{2}.nums)))
end

end

function [] = dispBottomNwords(v,i,train_data,words,x,N)

for j=1:1:N
   t=train_data{i(j)};
   fprintf('%i %f %f %f ||| ',j, v(j), t{3}, x(i(j)))
   fprintf('%s ||| %s\n',strjoin(words(t{1}.nums)),strjoin(words(t{2}.nums)))
end
end

function [x1,x2,y] = getScores(base,d1,addornn1,d2,addornn2,dataset)

load(base);
load(d1);
We1=We_orig;
if(addornn1==0)
    theta1=theta;
    [x1,y]=phrases_nn2(theta1,We1,25,words,dataset);
else
    [x1,y]=phrase_additive(We1,dataset);
end

load(d2);
We2=We_orig;
if(addornn2==0)
    theta2=theta;
    [x2,~]=phrases_nn2(theta2,We2,25,words,dataset);
else
    [x2,~]=phrase_additive(We2,dataset);
end


end