
def getPecentageOverlap(lines):
    n1 = 0
    n2 = 0
    for i in lines:
        i=i.split('|||')
        p1 = i[0]
        p2 = i[1]
        ct = 0
        w1 = p1.split()[0]
        w2 = p1.split()[1]
        w3 = p2.split()[0]
        w4 = p2.split()[1]
        if(w1 == w3):
            ct += 1
        if(w2 == w4):
            ct += 1
        if(ct == 1):
            n1 += 1
        if(ct == 2):
            n2 += 1
    return n1,n2

dir = '../../Paraphrase_Project_data/temp/'

f = open(dir+'adj-noun-base-xl.txt-all','r')
lines = f.readlines()
n1,n2 = getPecentageOverlap(lines)
print len(lines), n1, n2, float(n1)/len(lines)

f = open(dir+'noun-noun-base-xl.txt-all','r')
lines = f.readlines()
n1,n2 = getPecentageOverlap(lines)
print len(lines), n1, n2, float(n1)/len(lines)

f = open(dir+'verb-noun-base-xl.txt-all','r')
lines = f.readlines()
n1,n2 = getPecentageOverlap(lines)
print len(lines), n1, n2, float(n1)/len(lines)