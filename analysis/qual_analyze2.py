
def overlapscore(s1,s2,d):
    s1=s1.split()
    s2=s2.split()
    ct = 0
    l = len(s1)
    arrs=s1
    arrl=s2
    if len(s2) < l:
        l = len(s2)
        arrs=s2
        arrl=s1
    for i in arrs:
        for j in arrl:
            if i==j:
                ct += 1
                break
            if i in d:
                t = d[i]
                if j in t:
                    ct += 1
                    break
    return float(ct)/l

f=open('output.txt','r')
lines=f.readlines()

d={}
f2=open('/Users/Wieting/Desktop/Paraphrase_Project_data/word_data_done/words.txt','r')
lines2=f2.readlines()

for li in lines2:
    li=li.split()
    w1 = li[0]
    w2 = li[1]
    if w1 not in d:
        t=[]
        t.append(w2)
        d[w1]=t
    else:
        t = d[w1]
        t.append(w2)
        d[w1]=t
    if w2 not in d:
        t=[]
        t.append(w1)
        d[w2]=t
    else:
        t = d[w2]
        t.append(w1)
        d[w2]=t

avgpos1=0
avgneg1=0
totpos1=0
totneg1=0

avgpos2=0
avgneg2=0
totpos2=0
totneg2=0

outputs=[]
for li in lines:
    i=li.split('\t')
    if(abs(float(i[1])) >= 1):
        if(float(i[2].split()[2]) <2 or float(i[2].split()[2]) > 4):
            s1 = i[2].split('|||')[1]
            s2 = i[2].split('|||')[2]
            print i[1], i[2].split()[2], s1, s2, overlapscore(s1,s2,d), i[2].split()[1], i[3].split()[1]
            if(abs(float(i[1])) >= 1.5):
                outputs.append(s1.strip()+" & "+s2.strip()+" & "+i[2].split()[2]+" & "+i[2].split()[3]+" & "+i[3].split()[3])
            if float(i[2].split()[2]) <2 and float(i[1]) > 0:
                avgneg1 += overlapscore(s1,s2,d)
                totneg1 += 1
            if float(i[2].split()[2]) <2 and float(i[1]) < 0:
                avgneg2 += overlapscore(s1,s2,d)
                totneg2 += 1
            if float(i[2].split()[2]) > 4 and float(i[1]) > 0:
                avgpos1 += overlapscore(s1,s2,d)
                totpos1 += 1
            if float(i[2].split()[2]) > 4 and float(i[1]) < 0:
                avgpos2 += overlapscore(s1,s2,d)
                totpos2 += 1

'''
for i in outputs:
    i=i.split('&')
    d1 = float(i[2])
    d2 = float(i[3])
    d3 = float(i[4])
    s = i[0]+" & "+i[1]+" & "+str(round(d1,2))+" & "+str(round(d2,2))+" & "+str(round(d3,2))+"\\\\"
    print s


print float(avgneg1)/totneg1 #percent of overlap in negative examples that RNN does better
print float(avgneg2)/totneg2
print float(avgpos1)/totpos1
print float(avgpos2)/totpos2
'''


