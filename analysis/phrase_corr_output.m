function [] = phrase_corr(base,d1,addornn1,d2,addornn2,dataset,fid,name)

load(base);
load(d1);
We1=We_orig;
if(addornn1==0)
    theta1=theta;
    [x1,y]=phrases_nn2(theta1,We1,25,words,dataset);
else
    [x1,y]=phrase_additive(We1,dataset);
end

load(d2);
We2=We_orig;
if(addornn2==0)
    theta2=theta;
    [x2,~]=phrases_nn2(theta2,We2,25,words,dataset);
else
    [x2,~]=phrase_additive(We2,dataset);
end

c1=corr(x1',y','Type','Spearman');
c2=corr(x2',y','Type','Spearman');
c3=corr(x1',x2','Type','Spearman');
l=length(x1);
fprintf(fid,'%s\t%f\t%f\t%f\t%f\n',sprintf(name),c1,c2,c3,l);
[x1' y'];
end
