
def getLabelDifficulty(lines):
    error_ones=0
    error_twos=0
    error_threes=0
    error_fours=0
    ones=0
    twos=0
    threes=0
    fours=0
    for i in lines :
        #print i.strip()
        if "|||" not in i:
            continue
        l=i.split()
        error = float(l[1])
        score = float(l[2])
        if (score < 2):
            error_ones += error
            ones += 1
        elif (score < 3):
            error_twos += error
            twos += 1
        elif (score < 4):
            error_threes += error
            threes += 1
        else:
            error_fours += error
            fours += 1
#print error_ones/ones, error_twos/twos, error_threes/threes, error_fours/fours
    print round(error_ones/ones,2), round(error_twos/twos,2), round(error_threes/threes,2), round(error_fours/fours,2)
    print ones, twos, threes, fours


def getOverlapDifficulty(lines,d,neg):
    error_25=0
    error_50=0
    error_75=0
    error_one=0
    t_25=0
    t_50=0
    t_75=0
    t_1=0
    for i in lines :
        #print i.strip()
        if "|||" not in i:
            continue
        l=i.split()
        error = float(l[1])
        score = float(l[2])
        if(score >= 2 and score < 4):
            continue
        if(neg > 0 and score < 3):
            continue
        if(neg < 0 and score > 3):
            continue
        s1 = i.split("|||")[1].strip()
        s2 = i.split("|||")[2].strip()
        score = overlapscore(s1,s2,d)
        if (score <= 1./3 ):
            error_25 += error
            t_25 += 1
        elif (score >= 2./3 ):
            error_50 += error
            t_50 += 1
        else:
            error_75 += error
            t_75 += 1
    print round(error_25/t_25,2), round(error_50/t_50,2),round(error_75/t_75,2)
    print t_25, t_50, t_75
    return round(error_25/t_25,2), round(error_50/t_50,2),round(error_75/t_75,2)

def overlapscore(s1,s2,d):
    s1=s1.split()
    s2=s2.split()
    ct = 0
    l = len(s1)
    arrs=s1
    arrl=s2
    if len(s2) < l:
        l = len(s2)
        arrs=s2
        arrl=s1
    for i in arrs:
        for j in arrl:
            if i==j:
                ct += 1
                break
            if i in d:
                t = d[i]
                if j in t:
                    ct += 1
                    break
    return float(ct)/l

#make dictionary
f=open('output.txt','r')
lines=f.readlines()

d={}
f2=open('/Users/Wieting/Desktop/Paraphrase_Project_data/word_data_done/words.txt','r')
lines2=f2.readlines()

for li in lines2:
    li=li.split()
    w1 = li[0]
    w2 = li[1]
    if w1 not in d:
        t=[]
        t.append(w2)
        d[w1]=t
    else:
        t = d[w1]
        t.append(w2)
        d[w1]=t
    if w2 not in d:
        t=[]
        t.append(w1)
        d[w2]=t
    else:
        t = d[w2]
        t.append(w1)
        d[w2]=t


f = open('qual_results/25_sl999_ppdb.txt')
lines1 = f.readlines()

f = open('qual_results/25_sl999_rnn_ppdb.txt')
lines2 = f.readlines()

print "ADD <2, <3, <4, <=5"
getLabelDifficulty(lines1)
print "RNN <2, <3, <4, <=5"
getLabelDifficulty(lines2)
print ""
print "ADD-POS <.25, <.50, <.75, <=1"
a1,a2,a3=getOverlapDifficulty(lines1,d,1)
print "RNN-POS <.25, <.50, <.75, <=1"
r1,r2,r3=getOverlapDifficulty(lines2,d,1)
print round((r1-a1)/a1*100,2),round((r2-a2)/a2*100,2),round((r3-a3)/a3*100,2)
print ""
print "ADD-NEG <.25, <.50, <.75, <=1"
a1,a2,a3=getOverlapDifficulty(lines1,d,-1)
print "RNN-NEG <.25, <.50, <.75, <=1"
r1,r2,r3=getOverlapDifficulty(lines2,d,-1)
print round((r1-a1)/a1*100,2),round((r2-a2)/a2*100,2),round((r3-a3)/a3*100,2)
print ""
print "ADD-ALL <.25, <.50, <.75, <=1"
a1,a2,a3=getOverlapDifficulty(lines1,d,0)
print "RNN-ALL <.25, <.50, <.75, <=1"
r1,r2,r3=getOverlapDifficulty(lines2,d,0)
print round((r1-a1)/a1*100,2),round((r2-a2)/a2*100,2),round((r3-a3)/a3*100,2)

