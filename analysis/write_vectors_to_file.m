fout = fopen('paragram_vectors.txt','w');

load('params/skipwiki25.mat');
load('params/skipwiki25_1e-05_250_words-xl.params63.mat.ws353sl99.mat');

for i=1:1:length(We_orig)
   word = words{i};
   vec = We_orig(:,i);
   fprintf(fout,'%s',word);
   for j=1:1:length(vec)
       fprintf(fout,'\t%f',vec(j));
   end
   fprintf(fout,'\n');
end

fclose(fout);
