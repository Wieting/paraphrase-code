
def getDistortionDifficulty(lines,d,neg):
    error_0=0
    error_1=0
    error_2=0
    t_0=0
    t_1=0
    t_2=0
    for i in lines :
        #print i.strip()
        if "|||" not in i:
            continue
        l=i.split()
        error = float(l[1])
        score = float(l[2])
        if(score > 2 and score < 4):
            continue
        if(neg > 0 and score <= 2):
            continue
        if(neg < 0 and score >= 4):
            continue
        s1 = i.split("|||")[1].strip()
        s2 = i.split("|||")[2].strip()
        score = overlapscore(s1,s2,d)
        if(score < 0.5):
            continue
        score = getdistortionscore(s1,s2,d)
        #print score
        if (score == 0):
            error_0 += error/(len(s1)+len(s2))
            t_0 += 1
        elif (score <= 1):
            error_1 += error/(len(s1)+len(s2))
            t_1 += 1
        else:
            error_2 += error/(len(s1)+len(s2))
            t_2 += 1
    print round(error_0/t_0*100,2), round(error_1*100/t_1,2), round(error_2*100/t_2,2)
    print t_0, t_1, t_2
    return round(error_0/t_0*100,2), round(error_1*100/t_1,2), round(error_2*100/t_2,2)


def getdistortionscore(s1,s2,d):
    s1=s1.split()
    s2=s2.split()
    l = len(s1)
    arrs=s1
    arrl=s2
    if len(s2) < l:
        l = len(s2)
        arrs=s2
        arrl=s1
    ct = 0
    tot = 0
    for (ni,i) in enumerate(arrs):
        for (nj,j) in enumerate(arrl):
            if i==j:
                ct += 1
                tot += abs(ni-nj)
                break
            if i in d:
                t = d[i]
                if j in t:
                    ct += 1
                    tot += abs(ni-nj)
                    break
    return float(tot)/ct

def overlapscore(s1,s2,d):
    s1=s1.split()
    s2=s2.split()
    ct = 0
    l = len(s1)
    arrs=s1
    arrl=s2
    if len(s2) < l:
        l = len(s2)
        arrs=s2
        arrl=s1
    for i in arrs:
        for j in arrl:
            if i==j:
                ct += 1
                break
            if i in d:
                t = d[i]
                if j in t:
                    ct += 1
                    break
    return float(ct)/l

#make dictionary
f=open('output.txt','r')
lines=f.readlines()

d={}
f2=open('/Users/Wieting/Desktop/Paraphrase_Project_data/word_data_done/words.txt','r')
lines2=f2.readlines()

for li in lines2:
    li=li.split()
    w1 = li[0]
    w2 = li[1]
    if w1 not in d:
        t=[]
        t.append(w2)
        d[w1]=t
    else:
        t = d[w1]
        t.append(w2)
        d[w1]=t
    if w2 not in d:
        t=[]
        t.append(w1)
        d[w2]=t
    else:
        t = d[w2]
        t.append(w1)
        d[w2]=t


f = open('qual_results/25_sl999_ppdb.txt')
lines1 = f.readlines()

f = open('qual_results/25_sl999_rnn_ppdb.txt')
lines2 = f.readlines()

print "ADD-POS ==0, <1, <2, >= 2"
a1,a2,a3=getDistortionDifficulty(lines1,d,1)
print "RNN-POS ==0, <1, <2, >= 2"
r1,r2,r3=getDistortionDifficulty(lines2,d,1)
print round((r1-a1)/a1*100,2),round((r2-a2)/a2*100,2),round((r3-a3)/a3*100,2)
print ""
print "ADD-NEG ==0, <1, <2, >= 2"
a1,a2,a3=getDistortionDifficulty(lines1,d,-1)
print "RNN-NEG ==0, <1, <2, >= 2"
r1,r2,r3=getDistortionDifficulty(lines2,d,-1)
print round((r1-a1)/a1*100,2),round((r2-a2)/a2*100,2),round((r3-a3)/a3*100,2)
print ""
print "ADD-ALL ==0, <1, <2, >= 2"
a1,a2,a3=getDistortionDifficulty(lines1,d,0)
print "RNN-ALL ==0, <1, <2, >= 2"
r1,r2,r3=getDistortionDifficulty(lines2,d,0)
print round((r1-a1)/a1*100,2),round((r2-a2)/a2*100,2),round((r3-a3)/a3*100,2)
"""
print "ADD-POS ==0, <1, <2, >= 2"
a1,a2,a3,a4=getDistortionDifficulty(lines1,d,1)
print "RNN-POS ==0, <1, <2, >= 2"
r1,r2,r3,r4=getDistortionDifficulty(lines2,d,1)
print round((r1-a1)/a1*100,2),round((r2-a2)/a2*100,2),round((r3-a3)/a3*100,2),round((r4-a4)/a4*100,2)
print ""
print "ADD-NEG ==0, <1, <2, >= 2"
a1,a2,a3,a4=getDistortionDifficulty(lines1,d,-1)
print "RNN-NEG ==0, <1, <2, >= 2"
r1,r2,r3,r4=getDistortionDifficulty(lines2,d,-1)
print round((r1-a1)/a1*100,2),round((r2-a2)/a2*100,2),round((r3-a3)/a3*100,2),round((r4-a4)/a4*100,2)
print ""
print "ADD-ALL ==0, <1, <2, >= 2"
a1,a2,a3,a4=getDistortionDifficulty(lines1,d,0)
print "RNN-ALL ==0, <1, <2, >= 2"
r1,r2,r3,r4=getDistortionDifficulty(lines2,d,0)
print round((r1-a1)/a1*100,2),round((r2-a2)/a2*100,2),round((r3-a3)/a3*100,2),round((r4-a4)/a4*100,2)

print "ADD-POS ==0, <1, <2, >= 2"
getDistortionDifficulty(lines1,d,1)
print "RNN-POS <.25, <.50, <.75, <=1"
getDistortionDifficulty(lines2,d,1)
print ""
print "ADD-NEG ==0, <1, <2, >= 2"
getDistortionDifficulty(lines1,d,-1)
print "RNN-NEG ==0, <1, <2, >= 2"
getDistortionDifficulty(lines2,d,-1)
print ""
print "ADD-ALL ==0, <1, <2, >= 2"
getDistortionDifficulty(lines1,d,0)
print "RNN-ALL ==0, <1, <2, >= 2"
getDistortionDifficulty(lines2,d,0)
"""
