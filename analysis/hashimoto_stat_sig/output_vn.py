from scipy.stats import spearmanr
from random import random
import numpy as np

def getWordVec(w,vocab,vects):
    idx=-1
    for j in range(len(vocab)):
        ww=vocab[j]
        if ww==w:
            return vects[j]

def getPhraseVec(p,vocab,vects):
    p=p.split()
    w1 = p[0]+'__VB'
    v1=getWordVec(w1,vocab,vects)
    w2 = p[1]+'__NN'
    v2=getWordVec(w2,vocab,vects)
    v1 = np.array(v1)
    v2 = np.array(v2)
    #print v1
    #print v2
    #print v1+v2
    #print np.tanh(v1+v2)
    #return np.tanh(v1+v2)
    return v1+v2

def getPhraseVecNL(p,vocab,vects):
    p=p.split()
    w1 = p[0]+'__VB'
    v1=getWordVec(w1,vocab,vects)
    w2 = p[1]+'__NN'
    v2=getWordVec(w2,vocab,vects)
    v1 = np.array(v1)
    v2 = np.array(v2)
    #print v1
    #print v2
    #print v1+v2
    #print np.tanh(v1+v2)
    return np.tanh(v1+v2)

def getScoreNL(p1,p2,vocab,vects):
    p1=getPhraseVecNL(p1,vocab,vects)
    p2=getPhraseVecNL(p2,vocab,vects)
    v=np.dot(p1,p2)/(np.linalg.norm(p1)*np.linalg.norm(p2))
    return v

def getScoreL(p1,p2,vocab,vects):
    p1=getPhraseVec(p1,vocab,vects)
    p2=getPhraseVec(p2,vocab,vects)
    v=np.dot(p1,p2)/(np.linalg.norm(p1)*np.linalg.norm(p2))
    return v



#######
#NL
#######

def score_all_l(wvf,testf):
    f = open(wvf,'r')
    lines = f.readlines()
    vocab=[]
    vects=[]
    for i in lines:
        j=i.split()
        vocab.append(j[0])
        v=[]
        for i in range(len(j)):
            if(i==0):
                continue
            v.append(float(j[i]))
        vects.append(v)
    #print len(lines)
    #print len(vocab)
    #print len(vects)
    f = open(testf,'r')
    lines=f.readlines()
    gold=[]
    pred=[]
    for i in range(len(lines)):
        if i==0:
            continue
        l=lines[i]
        l=l.split('\t')
        gold.append(float(l[2]))
        pred.append(getScoreL(l[0],l[1],vocab,vects))
    #print len(pred)
    #print len(gold)
    return spearmanr(pred,gold), pred, gold

def score_all_nl(wvf,testf):
    f = open(wvf,'r')
    lines = f.readlines()
    vocab=[]
    vects=[]
    for i in lines:
        j=i.split()
        vocab.append(j[0])
        v=[]
        for i in range(len(j)):
            if(i==0):
                continue
            v.append(float(j[i]))
        vects.append(v)
    #print len(lines)
    #print len(vocab)
    #print len(vects)
    f = open(testf,'r')
    lines=f.readlines()
    gold=[]
    pred=[]
    for i in range(len(lines)):
        if i==0:
            continue
        l=lines[i]
        l=l.split('\t')
        gold.append(float(l[2]))
        pred.append(getScoreNL(l[0],l[1],vocab,vects))
    #print len(pred)
    #print len(gold)
    return spearmanr(pred,gold), pred, gold
    #for i in range(len(pred)):
        #print pred[i], gold[i]


fnl='/Users/Wieting/Desktop/Paraphrase_Project_data/temp/PAS-CLBLM.params.50dim/PAS-CLBLM(Add_nl).wv'
fl='/Users/Wieting/Desktop/Paraphrase_Project_data/temp/PAS-CLBLM.params.50dim/PAS-CLBLM(Add_l).wv'
fall = '/Users/Wieting/Desktop/Paraphrase_Project_code/evaluation/bigrams/data/all_verb-noun.txt'
ftest = '/Users/Wieting/Desktop/Paraphrase_Project_code/evaluation/bigrams/data/test_verb-noun.txt'

c,p,g=score_all_l(fl,ftest)
best_c_ml = c
best_p_ml = p
best_g_ml = g

c,p,g=score_all_nl(fnl,ftest)
if c > best_c_ml:
    best_c_ml = c
    best_p_ml = p
    best_g_ml = g

fall = '/Users/Wieting/Desktop/Paraphrase_Project_code/evaluation/bigrams/data/kj_vn.txt'

c,p,g=score_all_l(fl,fall)
best_c_kj = c
best_p_kj = p
best_g_kj = g

c,p,g=score_all_nl(fnl,fall)
if c > best_c_kj:
    best_c_kj = c
    best_p_kj = p
    best_g_kj = g

fout = open('outputs/ml_vn/ml_vn.txt','w')
print best_c_ml
fout.write(str(best_c_ml)+"\n")
for i in best_p_ml:
    #print i
    fout.write(str(i)+"\n")

fout = open('outputs/kj_vn/kj_vn.txt','w')
fout.write(str(best_c_kj)+"\n")
print best_c_kj
for i in best_p_kj:
    #print i
    fout.write(str(i)+"\n")
