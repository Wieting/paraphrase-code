
addpath('../../evaluation/bigrams/');
addpath('../../evaluation/wordsim/');
addpath('../../evaluation/semeval/');
addpath('../../evaluation/phrases/');
addpath('../../single_W_code/code/core/');

skip25='../params/skipwiki25.mat';
skipsl999='../params/skipwiki25_1e-05_250_words-xl.params63.mat.ws353sl99.mat';

skipsl999RNNnn='../params/skipwiki25SL999_0.001_0.001_100_noun-noun-xl.params25.nn.mat';
skipsl999RNNvn='../params/skipwiki25SL999_0.1_10_250_noun-noun-xl.params5.vn.mat';
skipsl999RNNjn='../params/skipwiki25SL999_0.01_0.1_2000_adj-noun-xl.params13.jn.mat';

kj_nn='../../evaluation/bigrams/data/kj_nn.txt';
kj_vn='../../evaluation/bigrams/data/kj_vn.txt';
kj_jn='../../evaluation/bigrams/data/kj_an.txt';

ml_jn='../../evaluation/bigrams/data/test_adj-noun.txt';
ml_nn='../../evaluation/bigrams/data/test_noun-noun.txt';
ml_vn='../../evaluation/bigrams/data/test_verb-noun.txt';

skipsl999RNNmlnn='../params/mlnn_skipwiki25SL999_0.001_0.001_250_noun-noun-xl.params16.mat';
skipsl999RNNmlvn='../params/mlvn_skipwiki25SL999_0.1_10_250_noun-noun-xl.params5.mat';
skipsl999RNNmljn='../params/mljn_skipwiki25SL999_0.01_0.1_2000_adj-noun-xl.params13.mat';


bigram_output(skip25,skipsl999,1,kj_nn,'outputs/kj_nn/SL999_kj_nn.txt');
bigram_output(skip25,skipsl999RNNnn,0,kj_nn,'outputs/kj_nn/SL999RNN_kj_nn.txt');

bigram_output(skip25,skipsl999,1,kj_jn,'outputs/kj_jn/SL999_kj_jn.txt');
bigram_output(skip25,skipsl999RNNjn,0,kj_jn,'outputs/kj_jn/SL999RNN_kj_jn.txt');

bigram_output(skip25,skipsl999,1,kj_vn,'outputs/kj_vn/SL999_kj_vn.txt');
bigram_output(skip25,skipsl999RNNvn,0,kj_vn,'outputs/kj_vn/SL999RNN_kj_vn.txt');

bigram_output(skip25,skipsl999,1,ml_vn,'outputs/ml_vn/SL999_ml_vn.txt');
bigram_output(skip25,skipsl999RNNmlvn,0,ml_vn,'outputs/ml_vn/SL999RNN_ml_vn.txt');

bigram_output(skip25,skipsl999,1,ml_jn,'outputs/ml_jn/SL999_ml_jn.txt');
bigram_output(skip25,skipsl999RNNmljn,0,ml_jn,'outputs/ml_jn/SL999RNN_ml_jn.txt');

bigram_output(skip25,skipsl999,1,ml_nn,'outputs/ml_nn/SL999_ml_nn.txt');
bigram_output(skip25,skipsl999RNNmlnn,0,ml_nn,'outputs/ml_nn/SL999RNN_ml_nn.txt');
