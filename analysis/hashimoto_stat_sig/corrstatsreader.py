from corrstats import *

#print dependent_corr(.396, .179, .088, 200, twotailed=True, conf_level=0.95, method='steiger')
#print dependent_corr(.396, .179, .088, 200, twotailed=True, conf_level=0.95, method='zou')

#print dependent_corr(.396, .179, .088, 200, twotailed=False, conf_level=0.95, method='steiger')
#print dependent_corr(.396, .179, .088, 200, twotailed=True, conf_level=0.90, method='zou')

f=open('statdata.txt','r')
lines = f.readlines()

for i in lines:
    i=i.split()
    n = i[0]+"\t"+i[1]+"\t"+i[2]
    n1=float(i[3])
    n2=float(i[4])
    n3=float(i[5])
    n4=float(i[6])
    print n, dependent_corr(n1, n2, n3, n4, twotailed=True, conf_level=0.95, method='steiger')
    print n, dependent_corr(n1, n2, n3, n4, twotailed=False, conf_level=0.95, method='steiger')
    #print dependent_corr(n1, n2, n3, n4, twotailed=False,  conf_level=0.95, method='zou')
    print ""
