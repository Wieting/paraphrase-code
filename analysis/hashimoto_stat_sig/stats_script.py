from scipy.stats import spearmanr

def getVector(lines):
    v=[]
    for (n,i) in enumerate(lines):
        if n==0:
            continue
        v.append(float(i))
    return v

def printLines(s):
    hash = 'outputs/' + s + '/' + s + '.txt'
    v = 'outputs/' + s + '/SL999_' + s + '.txt'
    rnn = 'outputs/' + s + '/SL999RNN_' + s + '.txt'
    f = open(hash, 'r')
    lines = f.readlines()
    ss = lines[0].split(',')[0].replace('(', '')
    hc = float(ss)
    hv = getVector(lines)

    f = open(v,'r')
    lines = f.readlines()
    slc = float(lines[0].split()[1])
    slv = getVector(lines)

    f = open(rnn,'r')
    lines = f.readlines()
    rnnc = float(lines[0].split()[1])
    rnnv = getVector(lines)

    print "HASH SL999"+"\t"+s+"\t"+str(hc)+"\t"+str(slc)+"\t"+str(spearmanr(hv,slv)[0])+"\t"+str(len(hv))
    print "HASH RNN"+"\t"+s+"\t"+str(hc)+"\t"+str(rnnc)+"\t"+str(spearmanr(hv,rnnv)[0])+"\t"+str(len(hv))


s = 'kj_jn'
printLines(s)

printLines('kj_nn')
printLines('kj_vn')

printLines('ml_jn')
printLines('ml_nn')
printLines('ml_vn')

