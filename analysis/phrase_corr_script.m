fid = fopen('statdata.txt','w');

skip25='params/skipwiki25.mat';
skip1000='params/skipwiki1000.mat';
skip500='params/skipwiki500.mat';
skip200='params/skipwiki200.mat';
skipsl999='params/skipwiki25_1e-05_250_words-xl.params63.mat.ws353sl99.mat';
skipws353='params/skipwiki25_0.0001_100_words-xl.params70.mat.ws353.mat';
skipsl999RNN='params/skipwiki25SL999_0.001_0.0001_100_phrase_60k.params15.ppdbsl999.mat';
skip25RNN='params/skipwiki25_0.01_1e-05_1000_phrase_60k.params5.ppdb.mat';
ppdb1k='../core_data/ppdb_test.txt.mat';

skipsl999RNNnn='params/skipwiki25SL999_0.001_0.001_100_noun-noun-xl.params25.nn.mat';
skipsl999RNNvn='params/skipwiki25SL999_0.1_10_250_noun-noun-xl.params5.vn.mat';
skipsl999RNNjn='params/skipwiki25SL999_0.01_0.1_2000_adj-noun-xl.params13.jn.mat';

kj_nn='../evaluation/bigrams/data/kj_nn.txt';
kj_vn='../evaluation/bigrams/data/kj_vn.txt';
kj_jn='../evaluation/bigrams/data/kj_an.txt';

ml_jn='../evaluation/bigrams/data/test_adj-noun.txt';
ml_nn='../evaluation/bigrams/data/test_noun-noun.txt';
ml_vn='../evaluation/bigrams/data/test_verb-noun.txt';

skipsl999RNNmlnn='params/mlnn_skipwiki25SL999_0.001_0.001_250_noun-noun-xl.params16.mat';
skipsl999RNNmlvn='params/mlvn_skipwiki25SL999_0.1_10_250_noun-noun-xl.params5.mat';
skipsl999RNNmljn='params/mljn_skipwiki25SL999_0.01_0.1_2000_adj-noun-xl.params13.mat';

addpath('../evaluation/bigrams/');
addpath('../evaluation/wordsim/');
addpath('../evaluation/semeval/');
addpath('../evaluation/phrases/');
addpath('../single_W_code/code/core/');

phrase_corr_output(skip25,skip25,1,skipsl999,1,ppdb1k,fid,'skip25\tSL999\tPPDB');
phrase_corr_output(skip25,skipsl999,1,skipsl999RNN,0,ppdb1k,fid,'SL999\tRNN\tPPDB');
phrase_corr_output(skip25,skip200,1,skipsl999RNN,0,ppdb1k,fid,'skip200\tRNN\tPPDB');
phrase_corr_output(skip25,skip500,1,skipsl999RNN,0,ppdb1k,fid,'skip500\tRNN\tPPDB');
phrase_corr_output(skip25,skip1000,1,skipsl999RNN,0,ppdb1k,fid,'skip1000\tRNN\tPPDB');

bigram_corr_output(skip25,skip25,1,skipsl999,1,kj_nn,fid,'skip25\tSL999\tkj_nn');
bigram_corr_output(skip25,skip25,1,skipsl999RNNnn,0,kj_nn,fid,'skip25\tRNN\tkj_nn');
bigram_corr_output(skip25,skipsl999,1,skipsl999RNNnn,0,kj_nn,fid,'SL999\tRNN\tkj_nn');
bigram_corr_output(skip25,skip200,1,skipsl999RNNnn,0,kj_nn,fid,'skip200\tRNN\tkj_nn');
bigram_corr_output(skip25,skip500,1,skipsl999RNNnn,0,kj_nn,fid,'skip500\tRNN\tkj_nn');
bigram_corr_output(skip25,skip1000,1,skipsl999RNNnn,0,kj_nn,fid,'skip1000\tRNN\tkj_nn');

bigram_corr_output(skip25,skip25,1,skipsl999,1,kj_jn,fid,'skip25\tSL999\tkj_jn');
bigram_corr_output(skip25,skipsl999,1,skipsl999RNNjn,0,kj_jn,fid,'SL999\tRNN\tkj_jn');
bigram_corr_output(skip25,skip200,1,skipsl999RNNjn,0,kj_jn,fid,'skip200\tRNN\tkj_jn');
bigram_corr_output(skip25,skip500,1,skipsl999RNNjn,0,kj_jn,fid,'skip500\tRNN\tkj_jn');
bigram_corr_output(skip25,skip1000,1,skipsl999RNNjn,0,kj_jn,fid,'skip1000\tRNN\tkj_jn');

bigram_corr_output(skip25,skip25,1,skipsl999,1,kj_vn,fid,'skip25\tSL999\tkj_vn');
bigram_corr_output(skip25,skip25,1,skipsl999RNNvn,0,kj_vn,fid,'skip25\tRNN\tkj_vn');
bigram_corr_output(skip25,skipsl999,1,skipsl999RNNvn,0,kj_vn,fid,'SL999\tRNN\tkj_vn');
bigram_corr_output(skip25,skip200,1,skipsl999RNNvn,0,kj_vn,fid,'skip200\tRNN\tkj_vn');
bigram_corr_output(skip25,skip500,1,skipsl999RNNvn,0,kj_vn,fid,'skip500\tRNN\tkj_vn');
bigram_corr_output(skip25,skip1000,1,skipsl999RNNvn,0,kj_vn,fid,'skip1000\tRNN\tkj_vn');

bigram_corr_output(skip25,skip25,1,skipsl999,1,ml_vn,fid,'skip25\tSL999\tml_vn');
bigram_corr_output(skip25,skipsl999,1,skipsl999RNNmlvn,0,ml_vn,fid,'SL999\tRNN\tml_vn');
bigram_corr_output(skip25,skip200,1,skipsl999RNNmlvn,0,ml_vn,fid,'skip200\tRNN\tml_vn');
bigram_corr_output(skip25,skip500,1,skipsl999RNNmlvn,0,ml_vn,fid,'skip500\tRNN\tml_vn');
bigram_corr_output(skip25,skip1000,1,skipsl999RNNmlvn,0,ml_vn,fid,'skip1000\tRNN\tml_vn');

bigram_corr_output(skip25,skip25,1,skipsl999,1,ml_jn,fid,'skip25\tSL999\tml_jn');
bigram_corr_output(skip25,skipsl999,1,skipsl999RNNmljn,0,ml_jn,fid,'SL999\tRNN\tml_jn');
bigram_corr_output(skip25,skip200,1,skipsl999RNNmljn,0,ml_jn,fid,'skip200\tRNN\tml_jn');
bigram_corr_output(skip25,skip500,1,skipsl999RNNmljn,0,ml_jn,fid,'skip500\tRNN\tml_jn');
bigram_corr_output(skip25,skip1000,1,skipsl999RNNmljn,0,ml_jn,fid,'skip1000\tRNN\tml_jn');

bigram_corr_output(skip25,skip25,1,skipsl999,1,ml_nn,fid,'skip25\tSL999\tml_nn');
bigram_corr_output(skip25,skipsl999,1,skipsl999RNNmlnn,0,ml_nn,fid,'SL999\tRNN\tml_nn');
bigram_corr_output(skip25,skip200,1,skipsl999RNNmlnn,0,ml_nn,fid,'skip200\tRNN\tml_nn');
bigram_corr_output(skip25,skip500,1,skipsl999RNNmlnn,0,ml_nn,fid,'skip500\tRNN\tml_nn');
bigram_corr_output(skip25,skip1000,1,skipsl999RNNmlnn,0,ml_nn,fid,'skip1000\tRNN\tml_nn');

sl999_corr_output(skip25,skip25,skipsl999,fid,'skip25\tSL999\tsl999');
sl999_corr_output(skip25,skip1000,skipsl999,fid,'skip25\tSL999\tsl999');