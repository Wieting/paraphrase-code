alias matlab=/Applications/MATLAB_R2014a.app/bin/matlab

matlab -nodisplay -nodesktop -nojvm -nosplash -r "phrase_corr_script;quit"
python corrstatsreader.py statdata.txt