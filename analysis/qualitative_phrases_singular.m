function [ ] = qualitative_phrases_singular(base,d1,addornn1,dataset,f1)

fid1 = fopen(f1,'w');

if(nargin==4)
    fid1=1;
end

load(base);
load(dataset);
load(d1);
data = [train_data valid_data test_data];
We1=We_orig;
if(addornn1==0)
    theta1=theta;
    [x1,y]=phrases_nn2(theta1,We1,25,words,dataset);
else
    [x1,y]=phrase_additive(We1,dataset);
end

x1=(x1-min(x1)).*4/(max(x1)-min(x1))+1;
[v,i]=sort(abs(x1-y),'descend');
c1=corr(x1',y','Type','Spearman');
dispTopNwords(v,i,data,words,x1,length(x1),c1,fid1);
end

function [] = dispTopNwords(v,i,data,words,x,N,c,fid)

l=length(i);
for j=1:1:N
   fprintf(fid,'%i %f %f %f ||| ',j, v(l-j+1), data{i(l-j+1)}{3}, x(i(l-j+1)));
   fprintf(fid,'%s ||| %s\n',strjoin(words(data{i(l-j+1)}{1}.nums),' '),strjoin(words(data{i(l-j+1)}{2}.nums),' '));
end
fprintf(fid,'%f\n',c);

end