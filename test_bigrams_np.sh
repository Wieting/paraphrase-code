#test word code
echo "Testing bigram code"
cd bigram_code/training/
alias matlab=/Applications/MATLAB_R2014a.app/bin/matlab
matlab -nodisplay -nodesktop -nojvm -nosplash -r "train_single(0, 0, 0.05, 100, '../../../Paraphrase_Project_data/bigram_data/adj-noun-base-xl.txt.mat', 'adj-noun-xl', 'config1.m');quit"

cd ../..
