function [] = testParamsBigrams(fname,theta,We,hiddenSize,words)
    [~,fname,~] = fileparts(fname);
    acc = bigrams(theta,We,hiddenSize,words,'../../evaluation/bigrams/data/dev_verb-noun.txt');
    fprintf('dev_verb-noun.......%s.......%d\n',fname,acc);
    
    acc = bigrams(theta,We,hiddenSize,words,'../../evaluation/bigrams/data/dev_noun-noun.txt');
    fprintf('dev_noun-noun.......%s.......%d\n',fname,acc);
    
    acc = bigrams(theta,We,hiddenSize,words,'../../evaluation/bigrams/data/dev_adj-noun.txt');
    fprintf('dev_adj-noun.......%s.......%d\n',fname,acc);
    
    acc = bigrams(theta,We,hiddenSize,words,'../../evaluation/bigrams/data/test_verb-noun.txt');
    fprintf('test_verb-noun.......%s.......%d\n',fname,acc);
    
    acc = bigrams(theta,We,hiddenSize,words,'../../evaluation/bigrams/data/test_noun-noun.txt');
    fprintf('test_noun-noun.......%s.......%d\n',fname,acc);
    
    acc = bigrams(theta,We,hiddenSize,words,'../../evaluation/bigrams/data/test_adj-noun.txt');
    fprintf('test_adj-noun.......%s.......%d\n',fname,acc);

    acc = bigrams(theta,We,hiddenSize,words,'../../evaluation/bigrams/data_all/verb-noun_rank.txt');
    fprintf('all_verb-noun.......%s.......%d\n',fname,acc);

    acc = bigrams(theta,We,hiddenSize,words,'../../evaluation/bigrams/data_all/noun-noun_rank.txt');
    fprintf('all_noun-noun.......%s.......%d\n',fname,acc);

    acc = bigrams(theta,We,hiddenSize,words,'../../evaluation/bigrams/data_all/adj-noun_rank.txt');
    fprintf('all_adj-noun.......%s.......%d\n',fname,acc);

    acc = bigrams(theta,We,hiddenSize,words,'../../evaluation/bigrams/data_all/kj_vn.txt');
    fprintf('kj_verb-noun.......%s.......%d\n',fname,acc);

    acc = bigrams(theta,We,hiddenSize,words,'../../evaluation/bigrams/data_all/kj_nn.txt');
    fprintf('kj_noun-noun.......%s.......%d\n',fname,acc);

    acc = bigrams(theta,We,hiddenSize,words,'../../evaluation/bigrams/data_all/kj_an.txt');
    fprintf('kj_adj-noun.......%s.......%d\n',fname,acc);
end
