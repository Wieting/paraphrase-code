import os
import operator
import sys

n='o2898978'
if(len(sys.argv) > 1):
    n=sys.argv[1]

lis=os.listdir("SGE")
t=[]
for i in lis:
    if n in i:
        f=open('SGE/'+i,'r')
        lines=f.readlines()
        for j in lines:
            if('params' in j):
                t.append(j)

lines=t
ll=[]
d_vnt={}
d_vnd={}
d_jnt={}
d_jnd={}
d_nnd={}
d_nnt={}

for l in lines:
    i = l.split('.......')
    n = float(i[2])
    if('test_verb-noun'in l):
        d_vnt[l]=n
    if('dev_verb-noun'in l):
        d_vnd[l]=n
    if('test_adj-noun'in l):
        d_jnt[l]=n
    if('dev_adj-noun'in l):
        d_jnd[l]=n
    if('test_noun-noun'in l):
        d_nnt[l]=n
    if('dev_noun-noun'in l):
        d_nnd[l]=n


sorted_d_vnt = sorted(d_vnt.iteritems(), key=operator.itemgetter(1), reverse=True)
sorted_d_vnd = sorted(d_vnd.iteritems(), key=operator.itemgetter(1), reverse=True)
sorted_d_jnt = sorted(d_jnt.iteritems(), key=operator.itemgetter(1), reverse=True)
sorted_d_jnd = sorted(d_jnd.iteritems(), key=operator.itemgetter(1), reverse=True)
sorted_d_nnt = sorted(d_nnt.iteritems(), key=operator.itemgetter(1), reverse=True)
sorted_d_nnd = sorted(d_nnd.iteritems(), key=operator.itemgetter(1), reverse=True)

ct = 0
for i in sorted_d_vnt:
    ll.append(i)
    ct += 1
    if(ct > 5):
        break

ct = 0
for i in sorted_d_vnd:
    ll.append(i)
    ct += 1
    if(ct > 5):
        break

ct = 0
for i in sorted_d_jnt:
    ll.append(i)
    ct += 1
    if(ct > 5):
        break

ct = 0
for i in sorted_d_jnd:
    ll.append(i)
    ct += 1
    if(ct > 5):
        break

ct = 0
for i in sorted_d_nnt:
    ll.append(i)
    ct += 1
    if(ct > 5):
        break

ct = 0
for i in sorted_d_nnd:
    ll.append(i)
    ct += 1
    if(ct > 5):
        break

for i in ll:
    print i[0].strip()