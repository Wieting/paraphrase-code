function [] = train_multi(l1,l2,frac,output)
addpath(genpath('../../multi_W/'));
hiddenSize = 25;
params.etat=.05;
params.etaw=.5;
params.lambda_t=l1;
params.lambda_w=l2;
params.margin = 1;

load('../data/skipwiki25.mat');
load('../data/theta_init_25.mat');
load('../data/phrase_data.mat');

%use adagrad
params.epochs = 20;
params.data= phrase_data(1:round(frac*length(phrase_data)));

%t = {};
%for i=1:1:length(params.data)
%    l = params.data{i};
%    label = l{3};
%    if (label > 0.5)
%        t{end+1}= l;
%    end
%end
%params.data = t;

params.batchsize = round(.05*length(params.data));
fprintf('Training on %d instances.\n',length(params.data));

Nodemap = containers.Map();
NodeParams = [];

[adatheta, cost] = AGWords(Nodemap, theta, NodeParams, params, hiddenSize, words, We_orig, output)
end