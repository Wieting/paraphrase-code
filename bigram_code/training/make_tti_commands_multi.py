import sys
import os

outfile = "../../params/skipwiki25_phrases_100k_multi_"

lr1=0.05
lr2=0.50

lambda1 = [0, 0.01, 0.001, 0.0001]
lambda2 = [0.01, 0.001, 0.0001, 0.00001, 0.000001]


for i in lambda1:
    for j in lambda2:
        cmd = str(i)+", "+str(j)+", .005, '"+outfile+str(i)+"_"+str(j)+"'"
        cmd = "matlab -nodisplay -nodesktop -nojvm -nosplash -r \"train_multi("+cmd+");quit\""
        print cmd