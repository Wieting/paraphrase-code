c="config1.m"
o="../../models/skipwiki25_bigrams_"

python make_tti_commands_single.py ../../../Paraphrase_Project_data/bigram_data/adj-noun-base-xl.txt.mat adj-noun-xl 0.05 $c $o > adj-noun_test.txt
python make_tti_commands_single.py ../../../Paraphrase_Project_data/bigram_data/verb-noun-base-xl.txt.mat verb-noun-xl 0.05 $c $o  > verb-noun_test.txt
python make_tti_commands_single.py ../../../Paraphrase_Project_data/bigram_data/noun-noun-base-xl.txt.mat noun-noun-xl 0.05 $c $o  > noun-noun_test.txt

cat adj-noun_test.txt verb-noun_test.txt noun-noun_test.txt > all_test.txt

python make_tti_commands_single.py ../../../Paraphrase_Project_data/bigram_data/adj-noun-base-xl.txt.mat adj-noun-xl 1.0 $c $o > adj-noun.txt
python make_tti_commands_single.py ../../../Paraphrase_Project_data/bigram_data/verb-noun-base-xl.txt.mat verb-noun-xl 1.0 $c $o > verb-noun.txt
python make_tti_commands_single.py ../../../Paraphrase_Project_data/bigram_data/noun-noun-base-xl.txt.mat noun-noun-xl 1.0 $c $o > noun-noun.txt

cat adj-noun.txt verb-noun.txt noun-noun.txt > all.txt

python make_tti_commands_single_nowords.py ../../../Paraphrase_Project_data/bigram_data/adj-noun-base-xl.txt.mat adj-noun-xl 0.05 $c $o > adj-noun_np_test.txt
python make_tti_commands_single_nowords.py ../../../Paraphrase_Project_data/bigram_data/verb-noun-base-xl.txt.mat verb-noun-xl 0.05 $c $o  > verb-noun_np_test.txt
python make_tti_commands_single_nowords.py ../../../Paraphrase_Project_data/bigram_data/noun-noun-base-xl.txt.mat noun-noun-xl 0.05 $c $o  > noun-noun_np_test.txt

cat adj-noun_np_test.txt verb-noun_np_test.txt noun-noun_np_test.txt > all_np_test.txt

python make_tti_commands_single_nowords.py ../../../Paraphrase_Project_data/bigram_data/adj-noun-base-xl.txt.mat adj-noun-xl 1.0 $c $o > adj-noun_np.txt
python make_tti_commands_single_nowords.py ../../../Paraphrase_Project_data/bigram_data/verb-noun-base-xl.txt.mat verb-noun-xl 1.0 $c $o > verb-noun_np.txt
python make_tti_commands_single_nowords.py ../../../Paraphrase_Project_data/bigram_data/noun-noun-base-xl.txt.mat noun-noun-xl 1.0 $c $o > noun-noun_np.txt

cat adj-noun_np.txt verb-noun_np.txt noun-noun_np.txt > all_np.txt