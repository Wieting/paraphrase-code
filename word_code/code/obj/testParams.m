function [] = testParams(fname,We,words)
    acc = wordsim(We,words);
    fprintf('%s....%d....%d....%d....%d\n',fname,acc(1),acc(2),acc(3),acc(4));
end
